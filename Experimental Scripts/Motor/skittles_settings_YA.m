%% Skittles setting
function skittles_settings(blocks, mode, n_trials, resultPath, subjID, task_folder)
%% set globals
global SkitSet;
global SkitSetAngle;
global SkitSetTraj;
global status;
global BRP BRP_alt;                 % ball release parameters (actual and alternative)
global BCP;                         % ball collision parameters
global TRP;                         % target release parameters (?)
global resultData;
global resultDataAngle;
global resultDataTraj;
global t_trials;
global startingblock;
global vid_or_act;


status      = set_status;                   % a primitive state machine
BRP         = set_BallReleaseParameters;    % ball release (and result) parameters
BRP_alt     = set_BallReleaseParameters;    %alternative BRP has the same structure
BCP         = set_BallCollisionParameters;
TRP         = set_TargetReleaseParameters;

if startingblock==1 && strcmp(vid_or_act, 'a')
    resultData = set_resultData; %do I need this global?
    resultDataAngle  = set_resultData; %do I need this global?
    resultDataAngle.SkitSet = SkitSet;
    
    % Pseudorandomize combinations of direction alternative, initially selected trajectory and motor/visuomotor trials
    % 1st column: direction alternatives (1 = +vdiff, -1 = -vdiff)
    % 2nd column: initially selected trajectory (1 = left, 0 = right) OR
    % position of the correct lever
    % 3rd column: condition (0 = visuomotor, 1 = motor)

    combinations = [-1 0 0; 1 0 0; -1 1 0; 1 1 0;   1 1 1; 1 0 1; -1 1 1; -1 0 1]; % all possible combinations
    %combinations = [-1 0 1; 1 0 1; -1 1 1; 1 1 1;   1 1 1; 1 0 1; -1 1 1; -1 0 1]; % all possible combinations for motor condition only
    combinations = repmat(combinations,n_trials/8,1); % bring it to right lengths

    for b=1:blocks
        % alternative randomly to the left or right (-1 or 1)
        combinationsAngle = combinations(randperm(size(combinations,1)),:); % shuffle
        
        % Angles
        resultDataAngle.type1(b).directionAlternative = combinationsAngle(:,1);

        resultDataAngle.type1(b).unequalPosthit=zeros(1,n_trials);
        resultDataAngle.visual.type1(b).unequalPosthit=zeros(1,n_trials/2);

        % PoR: randomize the location of the correct lever
        leverPosition.actualAngle         = combinationsAngle(:,2)+1; % Should be 1 and 2
        leverPosition.alternativeAngle    = 3 - leverPosition.actualAngle;
        resultDataAngle.type1(b).leverPosition   = leverPosition;

        %Randomize also the colour of the two alternative trajectories
        colorTraces.actualTrace      = repmat([1;2],n_trials/2,1);
        randomOrder                  = randperm(n_trials);
        colorTraces.actualTrace      = colorTraces.actualTrace(randomOrder);
        colorTraces.alternativeTrace = 3 - colorTraces.actualTrace;
    end

    %and the same for the visual blocks
    if ~t_trials

        % Pseudorandomize combinations of direction alternative, initially selected trajectory and motor/visuomotor trials
        % 1st column: direction alternatives (1 = +vdiff, -1 = -vdiff)
        % 2nd column: initially selected trajectory (1 = left, 0 = right) OR
        % position of the correct lever

        combinations = [-1 0; -1 1; 1 0; 1 1]; % all possible combinations
        combinations = repmat(combinations,n_trials/4,1); % bring it to right lengths

        for b=1:blocks
            % alternative randomly to the left or right (-1 or 1)
            vis_combinationsAngle = combinations(randperm(size(combinations,1)),:); % shuffle

            % PoR: randomize the location of the correct lever
            leverPosition.actualAngle         = vis_combinationsAngle(:,2)+1; % Should be 1 and 2
            leverPosition.alternativeAngle    = 3 - leverPosition.actualAngle;


        end
    end

    % blind trials
    for b=1:blocks
        %     if t_trials
        if mode == 'p'
            resultDataAngle.block(b).blindTrials       = zeros(1,n_trials); % arbitrarily decided to use Traj version for pretraining
        else
             resultDataAngle.block(b).blindTrials        = combinationsAngle(:,3);


        end
        resultDataAngle.type1(b).errorTrials = zeros(1,n_trials);%trials that are marked during type2 task as wrong selection (space)
    end

    %color of cone in blind trials randomly determined
    if binornd(1,0.5)
        resultDataAngle.blindTrialColor = 1;
        SkitSetAngle.blindTrialConeColor= 1;
    else
        resultDataAngle.blindTrialColor = 2;
        SkitSetAngle.blindTrialConeColor= 2;

    end
else
    % When not starting from the beginning, load resultData-Variables
    % instead of initialising them newly.
    if strcmp(vid_or_act,'a')
        loadblock = startingblock-1;
    else
        loadblock = startingblock;
    end
    Everything_file = [resultPath filesep subjID filesep task_folder '/EverythingSkittles/SkittlesEverything_' subjID '_block_' num2str(loadblock) '.mat'];
    listOfVariables = who('-file', Everything_file);
    if ismember('resultDataAngle', listOfVariables)
        load(Everything_file,'resultDataAngle');
    end

    if ismember('resultData', listOfVariables)
        load(Everything_file,'resultData');
    end
end

end