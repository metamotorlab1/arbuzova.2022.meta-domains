%% SkittlesMatlab - version for the PD study (Oct 2019)
% (c) MetaMotorLab, Angles version by Polina Arbuzova
% designed for LabJack devices (LJ)
% streaming the input (S)
%% set globals
global useSound useDaq;
global SkitSet;
global SkitSetAngle;
global status;
global BRP BRP_alt;                 % ball release parameters (actual and alternative)
global BCP;                         % ball collision parameters
global TRP;                         % target release parameters (?)
global VASsettings;
global leverData leverDataIndex;    %LeverDataIndex says where in the leverData array the datapoint is to be stored
global GL;                          %includes the openGL drawing ..stuff for PTB
global win winCenter winRect;               %window handle for PTB (necessary for e.g. showMovingBall)
global session;                     %daq session
global trial;
global resultDataAngle;
global t_trials;
global c_trials
global debug;
global frameCount;
global visualBlock;
global block;
global breaks breaksBetweenBlocks;
global elbowYcoordsPixels;
global startingblock
global vid_or_act
global leftie


%% 
tic % for measuring how long it takes to do the task
%% user
user = 'polina';

if strcmp(user,'polina')
    homePath = '~/Dropbox/PhDs/Polina/YoungAdults/ExperimentalScripts/';
    addpath(genpath('~/Dropbox/PhDs/Polina/YoungAdults/'))
    addpath(genpath('~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/'))
    resultPath = '~/Dropbox/PhDs/Polina/YoungAdults/Data';
else
    strcmp(user,'metamotorlab')
    homePath = '~/Dropbox/PhDs/Polina/YoungAdults/ExperimentalScripts/';
    addpath(genpath('~/Dropbox/PhDs/Polina/YoungAdults/'))
    addpath(genpath('~/Dropbox/PhDs/Polina/SkittlesParameters/Skittles_Angles_and_Trajectories_Combined/'))
    resultPath = '~/Dropbox/PhDs/Polina/YoungAdults/Data';
end
cd(homePath)

%% flags
debug               = 0;
useDaq              = 1;                               %use lever through data acquisition toolbox (1) or keyboard and mouse (0)
useSound            = 0;

breaksBetweenBlocks = 1;                                % 1 yes 0 no

trials_pretraining       = 8;
trials_training          = 96;  % multiples of 8 % default - 48
trials_feedback          = 16;   % divided by 8 (1 block)
trials_confidence        = 8;
trials_per_block         = 64;  % will be divided by 8 % default 80 - 40 for VM and 40 for M
blocks                   = 5;  % default 5

task_folder              = 'Angles';

startingblock = 1;
vid_or_act = 'a';
leftie = 0; % make 1 if participant is left-handed
%% Subject settings

% switch nargin
%     case 1
%         subjID = input('Participants ID: ','s');
%     case 0
        mode = input('SKITTLES: Pretraining (p), Feedback (f), Feedback with Confidence (c), Training (t), Normal (n) : ','s');
        subjID = input('Participants ID: ','s');
% end


% while ~(strcmp(mode,'p')||strcmp(mode,'f')||strcmp(mode,'n')||strcmp(mode,'t')||strcmp(mode,'c'))
%     mode  = input('Training (t), Feedback (f), normal (n) : ','s');
% end

if mode == 'p'
    t_trials = 0;
    c_trials = 0;
    feedback = 0;
    blocks=1;
    n_trials = trials_pretraining;
    %     while exist([resultPath filesep subjID '/SkittlesPreTrainingResult_' subjID,'.mat'],'file')
    %         subjID = input('Participants ID already taken: ','s');
    %     end
    disp([int2str(n_trials) ' PRE-TRAINING trials are beginning'])
    
elseif mode == 't'
    t_trials = 1;
    c_trials = 0;
    feedback = 0;
    while exist([resultPath filesep subjID filesep task_folder '/SkittlesTrainingResult_' subjID,'.mat'],'file')
        subjID = input('Participants ID already taken: ','s');
    end
    n_trials = trials_training;
    blocks=1;
    disp([int2str(n_trials*blocks) ' TRAINING trials are beginning'])
    
elseif  mode=='f'
    t_trials = 0;
    c_trials = 0;
    feedback = 1;
    blocks=1;
    while exist([resultPath filesep subjID filesep task_folder '/SkittlesFeedbackResult_' subjID,'.mat'],'file')
        subjID = input('Participants ID already taken: ','s');
    end
    n_trials=trials_feedback;
    disp([int2str(n_trials*blocks) ' FEEDBACK trials are beginning'])
    
elseif  mode=='c'
    t_trials = 0;
    c_trials = 1;
    feedback = 1;
    blocks=1;
    while exist([resultPath filesep subjID filesep task_folder '/SkittlesConfFeedbackResult_' subjID,'.mat'],'file')
        subjID = input('Participants ID already taken: ','s');
    end
    n_trials=trials_confidence;
    disp([int2str(n_trials*blocks) ' FEEDBACK and CONFIDENCE trials are beginning'])
    
elseif  mode=='n'
    t_trials = 0;
    c_trials = 1;
    feedback = 0;
    while exist([resultPath filesep subjID filesep task_folder  '/EverythingSkittles'],'dir')
        subjID_candidate = input('Participants ID already taken. Start anyways (enter s) or different ID (enter an ID): ','s');
        % This allows to restart the experiment (e.g. after it crashed) from a chosen block and part
        if strcmp(subjID_candidate, 's')
            startingblock = 0;
            while ~ismember(startingblock,1:blocks)
                startingblock = input('From which block do you want to start?');
            end
            vid_or_act = '';
            while ~strcmp(vid_or_act,'a') &&~strcmp(vid_or_act,'v')
                vid_or_act = input('Start from active (a) or video (v) part?','s');
            end
            break
        else
            subjID = subjID_candidate;
        end
    end
    n_trials=trials_per_block;
    disp([int2str(n_trials*blocks) ' trials are beginning'])
end

if isempty(subjID); subjID = 'test'; end

if ~exist([resultPath filesep subjID filesep task_folder],'dir')
    mkdir([resultPath filesep subjID filesep task_folder])
end


%% initialize variables/arrays
resultDataAngle.useDaq = useDaq;
trial             = 1;                        %Initialize
 
% Initialize some Skittles Settings. The rest is initialized in
% skittles_settings function
if startingblock==1 && strcmp(vid_or_act,'a')
    SkitSet      = set_SkitSet_YA;                  %Set up the whole Skittles scene
    SkitSetAngle = set_SkitSet_YA;                  %Set up the whole Skittles scene
    breaks       = 0;
    visualBlock  = 0;
else
    if strcmp(vid_or_act,'a')
        loadblock = startingblock-1;
    else
        loadblock = startingblock;
    end
    load([resultPath filesep subjID filesep task_folder '/EverythingSkittles/SkittlesEverything_' subjID '_block_' num2str(loadblock) '.mat'],'SkitSet');
    load([resultPath filesep subjID filesep task_folder '/EverythingSkittles/SkittlesEverything_' subjID '_block_' num2str(loadblock) '.mat'],'SkitSetAngle');
    load([resultPath filesep subjID filesep task_folder '/EverythingSkittles/SkittlesEverything_' subjID '_block_' num2str(loadblock) '.mat'],'breaks');
    load([resultPath filesep subjID filesep task_folder '/EverythingSkittles/SkittlesEverything_' subjID '_block_' num2str(loadblock) '.mat'],'visualBlock');
end
%% Psychtoolbox settings

KbName('UnifyKeyNames');
whichScreen  = 0;

% For response box
devices = PsychHID('Devices'); % get all devices
device_manufacturer = {devices.manufacturer}; % find the manufacturer of them
responseBoxID = find(cellfun(@(x) strcmpi(x, 'Black Box Toolkit Ltd.'), device_manufacturer)); % get the ID number of the response box

kbNum = responseBoxID; % number of the input device

RestrictKeysForKbCheck(SkitSet.responseKeys);
bg           = 0.65;
grey     = round(255*[bg bg bg]);
SkitSet.bg_color=grey;

if ~useDaq
    elbowYcoordsPixels = 729; %929 if winRect 0 0 1000 1000; %529 if winRect 0 0 600 600
end
if debug
    
    Screen('Preference', 'SkipSyncTests', 1); %1 or 2->without testing
    %Screen('Preference', 'ConserveVRAM', 64); %ONLY FOR WINDOWS
    debuggingRect = [0 0 800 800];
    %Find (by hand) to the y pixel (in PTB coordinates) of the lever elbow
    %Will change if size of PTB's windowRect changes
    
else
    Screen('Preference', 'SkipSyncTests', 1);
    debuggingRect = [];
end

%% Init sound
if useSound
    init_sound;
end
%% Open window and specify some OpenGL settings
try
    %second argument (debuglevel) set to 0 to increase performance. Does not check after every GL call
    InitializeMatlabOpenGL( [], [], [], 0 );
    [win , winRect] = PsychImaging( 'OpenWindow', whichScreen, grey, debuggingRect, [], [], 0, 0 );
    
    Screen( 'BeginOpenGL', win );
    screenAspectRatio = winRect( 4 ) / winRect( 3 );
    winCenter.x = winRect(3)/2;
    winCenter.y = winRect(4)/2;
    glEnable( GL.LIGHTING );
    glEnable( GL.LIGHT0 );
    glEnable( GL.DEPTH_TEST );
    glMatrixMode( GL.PROJECTION );
    glLoadIdentity;
    gluPerspective( 25, 1 / screenAspectRatio, 0.1, 100 );
    glMatrixMode( GL.MODELVIEW );
    glLoadIdentity;
    %Set light source position. The original, cool-looking scene was with [ 1 2 5 0 ].
    % We changed this because the hue of the lever depended on the angle: not
    % good! [0 0 1 0] is boring but correct. [1 2 15 0] is a compromise that sort of works
    glLightfv( GL.LIGHT0,GL.POSITION, [ 0 0 1 0 ] );
    %Use gluLookAt to position the camera. Thre 9 arguments to the function
    %set:
    %-view point (0,0,8):     camera at the center of the scene, elevated 8 units on z
    %-center (0,0,0):         a reference point towards which the camera is aimed
    %-and direction (0,1,0):  indicate which direction is up.
    gluLookAt( 0, 0, 8, 0, 0, 0, 0, 1, 0);
    glClearColor( bg,bg,bg, 0 );
    Screen( 'EndOpenGL', win );
    %Screen( 'Flip', win, [], [], 0 );
    
    %This depends on winCenter, therefore all the way down here
    VASsettings = set_VASsettings;              %to draw (banded) confidence scale
    
    if ~debug
        HideCursor();
    end
    
    
    
    
    %% welcome screen
    welcomeScreen();
    
    %% Start data acquisition
    
    %leverDataIndex will be a counter, increased in 1 after each iteration of the get_leverData_1ms.m
    %It indicates the position of the datapoint in the leverData array
    leverDataIndex = 0;
    
    session = [];  %TODO: Do we need this?
    
    if useDaq
        dev=LJ_Handle;
        dev.scansPerRead=1;
        hz=dev.StreamRate;
        
        if dev.streamStarted %just to be sure that no stream is running
            dev.streamStop;
        end
        pause( 0.5 )
    else
        event.Angle = 0; %#ok<*UNRCH>
        hz=Screen('NominalFrameRate',win);
        dev=0; % just for the testing purposes
        if strcmp(user, 'elisa')
            hz = 60;
        end
        
    end
    
    % Preallocate leverData. size = timepoints x 3. Columns correspond to:
    %(1) time stamp;
    %(2) released ball (figer lifted from sensor or mouse pressed);
    %(3) lever angle
    %(4) lever data used for screen (Bool)
    leverData = zeros(SkitSet.leverDataPreallocateLength,4);
    if not(hz==SkitSet.FrameRate)
        SkitSet.FrameRate=hz;
        disp('Attention Measuring-Rate is at:')
        disp(SkitSet.FrameRate)
    end
    %% PTB main loop
    pause( .1 )
    
    frameCount   = 1;
    totalTime=GetSecs;
    
    
    experiment = 'a'; % ANGLES version of the experiment

    %% Skittles settings
    skittles_settings_YA(blocks, mode, n_trials, resultPath, subjID, task_folder);


    %% Initialize and pseudorandomize parameters(s) for the type1 question
    [aStepsize] = staircase_settings_skittles_angle_YA(mode, resultPath, subjID, task_folder, blocks);

    %% Run main part
    for block=startingblock:blocks
        
        %% Action condition
        if strcmp(vid_or_act,'a')
            if mode ~= 'p' %|| mode ~= 'f'

                trial_angle_YA(n_trials, mode, feedback, aStepsize, dev);

                % Show the target hits count after each block
                resultDataAngle.hits_all = show_target_hit_result_separate(resultDataAngle, block, n_trials);

            else % For pretraining trials
                trial_angle_YA(n_trials, mode, feedback, aStepsize, dev);  
            end
                   
            % save data if video trials will be following in this block
            if mode == 'n'
                if ~exist([resultPath filesep subjID filesep task_folder '/EverythingSkittles'],'dir')
                    mkdir([resultPath filesep subjID filesep task_folder '/EverythingSkittles']);
                end
                save([resultPath filesep subjID filesep task_folder '/SkittlesResult_' subjID, '_Angle_block_' num2str(block), '.mat'], 'resultDataAngle');
                save([resultPath filesep subjID filesep task_folder '/EverythingSkittles/SkittlesEverything_' subjID,'_block_' num2str(block), '.mat']);
            end
        else 
            vid_or_act = 'a'; % without this, it would always try to skip action trials even in later blocks
        end

        if block==blocks
            t_total = GetSecs-totalTime;
            resultDataAngle.totalTime=t_total;

        end

        %% Save
        resultDataAngle.useDaq = useDaq;

        if ~t_trials
            if mode == 'f'
                % For feedback trials
                save([resultPath filesep subjID filesep task_folder '/SkittlesFeedbackResult_' subjID, '.mat'], 'resultDataAngle');
            elseif mode == 'c'
                % For confidence with feedback trials
                save([resultPath filesep subjID filesep task_folder '/SkittlesConfFeedbackResult_' subjID, '.mat'], 'resultDataAngle');
            elseif mode == 'p'
                % For pre-training trials
                save([resultPath filesep subjID filesep task_folder '/SkittlesPreTrainingResult_' subjID, '.mat'], 'resultDataAngle');
            else
                if ~exist([resultPath filesep subjID filesep task_folder '/EverythingSkittles'],'dir')
                    mkdir([resultPath filesep subjID filesep task_folder '/EverythingSkittles']);
                end
                % For normal trials
                save([resultPath filesep subjID filesep task_folder '/SkittlesResult_' subjID, '_Angle_block_' num2str(block), '.mat'], 'resultDataAngle');
                save([resultPath filesep subjID filesep task_folder '/EverythingSkittles/SkittlesEverything_' subjID,'_block_' num2str(block), '.mat']);
            end
        else
            % For training trials
            save([resultPath filesep subjID filesep task_folder '/SkittlesTrainingResult_' subjID,'.mat'], 'resultDataAngle');
            % show the results of the training
            figure('Name','Training Results: Angles')
            hold on
            %subplot(subplotSet.rows,subplotSet.cols,subplotSet.position); hold on
            %title('staircase')
            plot(abs(resultDataAngle.type1.adiff(find(resultDataAngle.type1.whichStair==1))), ['r' '-o'])
            plot(abs(resultDataAngle.type1.adiff(find(resultDataAngle.type1.whichStair==2))), ['b' '-o'])
            
            xlabel('trial')
            ylabel('lever angle difference')
        end

    end
    
    if mode == 'n'
        text_size = 32; % text size for instructions
        if debug
            text_size = 14;
        end
        Screen('Flip', win);
        Screen('TextSize', win, text_size); % for the goodbye screen
        endMessage = ' Thank you for taking part! Press any button to quit. \n\n Vielen Dank fuer Ihre Teilnahme! Druecken Sie eine beliebige Taste, um den Vorgang zu beenden.';
        DrawFormattedText(win, endMessage, text_size,  winCenter.x*0.5, winCenter.y);    Screen('Flip', win);
        WaitSecs(.7);
        % Press any button to finish the experiment
        KbWait;
    end

    
catch e
    Screen( 'CloseAll' );
    ShowCursor();
    save([resultPath filesep subjID filesep task_folder '/SkittlesERRORTRIALS_' subjID,'.mat']);
    rethrow(e);
    %if useDaq; dev.close; end
end

%% Clean up
Screen('CloseAll' );
ShowCursor();

if useDaq; dev.close; end

clearvars -global leverData

if useSound
    PsychPortAudio( 'Stop',  pahandle_target );
    PsychPortAudio( 'Close', pahandle_target );
    PsychPortAudio( 'Stop',  pahandle_center );
    PsychPortAudio( 'Close', pahandle_center );
end
disp( ['mean frames per second = ', num2str( frameCount / t_total, '%3.2f' )] )

% print how lown it took to do the experiment 
time = toc/60; % in minutes
disp('');
disp(['This part of the experiment took: ', num2str(time), ' minutes.']);
 