function SkitSet = set_SkitSet
global leftie
%% settings for skittles task
%SkitSet.xTarget         = 0.0000;
%SkitSet.xTarget_alt1    = 0.0000;
%SkitSet.xTarget_alt2    = 0.2000;
%SkitSet.xTarget         = 0.9500;
SkitSet.xTarget         = 0.2000;
if leftie 
    SkitSet.xTarget         = -0.2000;
end
SkitSet.yTarget         = 0.7000;
SkitSet.xCenter         = 0;        %Used for glTranslatef - didn't quite get what it does
SkitSet.yCenter         = 0;
SkitSet.xLever          = 0;        %starting position

SkitSet.xLeverOffset    = 0.6000;

SkitSet.yLever          = -1.5000;  %In Heiko's coordinates, these are 'meters' and follow Mueller and Sternad (2004)
SkitSet.LeverLength     = 0.4000;
SkitSet.BallRadius      = 0.0500;
SkitSet.Mass            = 0.1000;
SkitSet.CenterRadius    = 0.2500;   %radius of the center pole (base or the cone drawn by openGL)
SkitSet.xStiff          = 1;        %Spring constant (k in Mueller and Sternad 2004) in x ...
SkitSet.yStiff          = 1;        %and y directions
SkitSet.Damping         = 0.0100;
SkitSet.xOmega          = 3.1619;   %this we know. Just the frequency, used for the trajectory x(t), y(t) calculation
SkitSet.yOmega          = 3.1619;
SkitSet.LeverWidth      = 0.0400;
SkitSet.max_fligth_time = 2;        % 2 sec is correct. using more? for debugging
SkitSet.FrameRate       = 1000;      %rate of lever data measurement 
SkitSet.dataPointsForVelocity = 6;
SkitSet.leverDataPreallocateLength = 15000; % 5000 was the original value
%SkitSet.leverDataLengthForStoring = 15000;


%% settings for type 1 and type 2 response
SkitSet.responseKeys = [KbName('a')  KbName('d') KbName('space') KbName('k') KbName('j') KbName('h') KbName('g') ];   %Will be used for type 1 and type 2 response
SkitSet.responseKeyNames = '<>';  %Will be displayed as text in type 1 response


SkitSet.response2leverRange=[60 120];
SkitSet.response1Selection= 45;% sensitivity for response type 1 selection (broadness of angle for selection of trajectory)
%     SkitSet.t_vdiff=1.5; % task 1 difficulty for t_trials

SkitSet.textColor=[0 0 0]; %black text display
SkitSet.coneColor(1,:)=[1 0 0 1]; % when alternating, default is 1 0 0 1 (when not, as in PD study, then 0 0 1 1
SkitSet.coneColor(2,:)=[0 0 1 1]; % used for alternating the center post color
SkitSet.blindTrialConeColor=0;

end

