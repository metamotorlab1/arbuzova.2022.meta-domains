function get_leverData_1ms_mouse_Angle_YA(event)
global leverData;
global leverDataIndex;
global status;
global resultDataAngle;
global SkitSet;
global BRP BRP_alt;
global block

%would be better to have trial as param but this should follow the form of
%the callback function for the DAQ. So get_leverData_etc should only depend
%on (event)
global trial

%Define the index to store the data in such a way that, if you go over the
%preallocated length, you start again from 1
leverDataIndex = mod( leverDataIndex, SkitSet.leverDataPreallocateLength ) + 1;
leverData( leverDataIndex, 1:3 ) = [event.TimeStamps, event.Buttons, event.Angle];
leverData( leverDataIndex, 4 ) = 0;

if status.ball.grabbed == 0
    if leverData( leverDataIndex, 2 ) == 1   %so if there's a button held down
        status.ball.grabbed = 1;
    end
end

if status.ball.grabbed == 1 && leverData( leverDataIndex, 2 ) == 0      % if ball was held and now is released
    status.ball.thrown = 1;
end

if status.ball.thrown == 1 && status.ball.flightPredicted == 0
    %calculate velocity by getting the angular difference over the last
    %(SkitSet.dataPointsForVelocity) datapoints (set to 6 initially)
    idx_start = leverDataIndex - SkitSet.dataPointsForVelocity;
    if idx_start < 1
        idx_start = mod( idx_start, SkitSet.leverDataPreallocateLength ) + 1;
    end
    
    %calculate distance traveled in m/sec. So we need length of circular
    %segment s. Wiki confirms that s = alpha/180 * pi * radius ->
    %distance in meters. This is exactly what this formula calculates.
    %Then, we need time and that's the second term: it calculates secs as
    %(Frames/secs)/frames needed to travel the section s
    v = pi * SkitSet.LeverLength * ( leverData( leverDataIndex, 3 ) - leverData( idx_start, 3 ) ) / 180 * SkitSet.FrameRate / SkitSet.dataPointsForVelocity;
    %Store Initial release parameters (i.e. the actual release parameters at t0)
    RP_t0.t0    = leverData( leverDataIndex, 1 );
    RP_t0.alpha = leverData( leverDataIndex, 3 );
    RP_t0.v     = v;
    RP_t0.leverDataIndex = leverDataIndex;
    
    %resultDataAngle.RP_t0 = RP_t0; <- PROBABLY UNNECCESSARY
    
    
    BRP = calculate_release_parameters(RP_t0.t0, RP_t0.alpha, RP_t0.v, RP_t0.leverDataIndex, BRP);
    
    %The first trajectory is given by BRP. Now get the second (alternative)
    %trajectory with staircased differences in the relevant parameter of
    %interest.
    
    resultDataAngle.block(block).RP_t0 = RP_t0;
    whichStair=resultDataAngle.type1(block).whichStair(trial);
    directionAlternative=resultDataAngle.type1(block).directionAlternative(trial);
    
    adiff   = resultDataAngle(whichStair).stairs.Signal *directionAlternative ;
    %    if mode ~= 'p'
    BRP_alt = calculate_release_parameters(RP_t0.t0, RP_t0.alpha + adiff, RP_t0.v, RP_t0.leverDataIndex, BRP_alt);
    
    
    resultDataAngle.type1(block).adiff(trial) = adiff;
    resultDataAngle.block(block).releaseSpeed(trial)=v;
    resultDataAngle.block(block).releaseAngle(trial)=RP_t0.alpha;
    
    status.ball_released = 1;
    status.ball.flightPredicted = 1;
    
end


