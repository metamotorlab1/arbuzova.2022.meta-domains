%% Trials loop for the Skittles Angles
function trial_angle_YA(n_trials, mode, feedback, aStepsize, dev)
%% set globals
global useDaq;
global SkitSet;
global status;
global BRP BRP_alt;                 % ball release parameters (actual and alternative)
global leverData leverDataIndex;    %LeverDataIndex says where in the leverData array the datapoint is to be stored
global win winCenter winRect;               %window handle for PTB (necessary for e.g. showMovingBall)
global trial;
global resultDataAngle;
global c_trials
global frameCount;
global block;
global elbowYcoordsPixels;


for trial = 1 : n_trials
    if useDaq
        dev.streamStart;
        %streamTime=GetSecs-startTime;
        streamTime=0;
        % tDiff=0;
    else
        startTime=GetSecs;
    end
    while status.task.motor
        
        Screen( 'BeginOpenGL', win );
        glClear;
        
        if ~useDaq
            [mouse.x, mouse.y, buttons] = GetMouse(win);
            
            dx = (winRect(3)/2 - mouse.x);
            dy = (elbowYcoordsPixels - mouse.y);
            %We call these event.etc to emulate the data acquisition output
            event.Angle      = atan2d(dy,dx);
            event.Buttons    = any(buttons);
            
            streamTime =GetSecs-startTime;
            event.TimeStamps = streamTime;
            
            get_leverData_1ms_mouse_Angle_YA(event);
            
        else
            [leverRead,backlog]=dev.streamRead;
            dataAv=true;
            while dataAv
                event.Angle=leverRead(2,1);
                event.Grip=leverRead(1,1);
                streamTime=streamTime+1/dev.StreamRate;
                event.TimeStamps=streamTime;
                % event.TimeStamps=leverRead(3,1)-tDiff;
                
                get_leverData_1ms_LJ_Angle_YA(event);
                
                if backlog<=dev.scansPerRead*2
                    dataAv=false;
                else
                    [leverRead,backlog]=dev.streamRead;
                end
            end
        end
        
        %% display the skittles scene
        
        angle = leverData(leverDataIndex, 3 );
        t     = leverData( leverDataIndex, 1 );
        leverData(leverDataIndex,4)=1;
        
        showSkittlesSceneAngle(angle, t, resultDataAngle.block(block).blindTrials(trial), mode);
        
        Screen( 'EndOpenGL', win );
        
        % Show rendered image at next vertical retrace
        Screen( 'Flip', win, [], [], 0 );
        frameCount=frameCount+1;
        
        %% Define states
        if t > BRP.t0 + SkitSet.max_fligth_time && status.ball.thrown
            %if leverData( leverDataIndex, 1 ) > BRP.t0 + SkitSet.max_fligth_time && status.ball.thrown
            status.task.motor = 0;
            status.task.type1 = 1;
            
            
            resultDataAngle.block(block).ballCollision(trial)=status.ball.hitTarget;
            
        end
        
    end
    
    %% do type 1 task
    %
    if mode ~= 'p'
        
        % Angles
        status = getType1ResponseBigLevers_PD(status, BRP, BRP_alt, trial, resultDataAngle.type1(block).leverPosition, feedback, c_trials, dev, streamTime);
                
        %TODO error trials remove!
        if ~resultDataAngle.type1(block).errorTrials(trial)
            %Determine which case you're gonna use
            resultDataAngle.stairs(resultDataAngle.type1(block).whichStair(trial)) = staircaseTrial(1, resultDataAngle.stairs(resultDataAngle.type1(block).whichStair(trial)), resultDataAngle.type1(block).correct(trial));
            resultDataAngle.stairs(resultDataAngle.type1(block).whichStair(trial)) = staircaseUpdate(1, resultDataAngle.stairs(resultDataAngle.type1(block).whichStair(trial)), -aStepsize);
            
        end
    end
    if useDaq
        dev.streamStop;
        pause( 0.0005 )
    end
    
    status.task.type1 = 0;
    
    
    
    %% do type 2 task
    %
    if mode ~= 'p'
        if c_trials %~t_trials
            status.task.type2 = 1;
            resultDataAngle = ConfidenceScaleDiscretePD(win, SkitSet, winCenter, resultDataAngle, feedback, trial, block); % instead of getType2Response function from the original Skittles experiment
            status.task.type2 = 0;
        end
        
        %  if t > BRP.t0 + SkitSet.max_fligth_time && status.ball_released  %TODO: is this if condition neccessary?!
        %                resultData = close_trial(trial, resultData);                        %reset all statuses
        %  end
    end
    resultDataAngle = close_trial(trial, resultDataAngle, mode);                        %reset all statuses
    % Plot angles data after closing the trial
    % plot(resultDataAngle.block(block).movementDataExtra(trial).angles)
    
end


end