function [aStepsize] = staircase_settings_skittles_angle_YA(mode, resultPath, subjID, task_folder, blocks)
global resultDataAngle
global startingblock
global vid_or_act

%% Initialize and pseudorandomize parameters(s) for the type1 question

%will have to move the lever to realistic min and max angle for the staircase
aDiff_max = 80;

if mode == 'n'
    %if ~t_trials
    %     if megablock==1
    if exist([resultPath filesep subjID filesep task_folder '/SkittlesTrainingResult_' subjID,'.mat'],'file')
        
        resultTrainingAngle = load([resultPath filesep subjID filesep task_folder '/SkittlesTrainingResult_' subjID,'.mat'], 'resultDataAngle');
        
        adiff_vm_staircase = resultTrainingAngle.resultDataAngle.type1.adiff((find(resultTrainingAngle.resultDataAngle.type1.whichStair==1)));
        aDiff_start_vm = 22;
        adiff_m_staircase = resultTrainingAngle.resultDataAngle.type1.adiff((find(resultTrainingAngle.resultDataAngle.type1.whichStair==2)));
        
        
        aDiff_start_vm = 22;
        aDiff_start_m = abs(adiff_m_staircase(end));
        
    else
        warning('No training data found!! Set staircase start to default. Hit space to continue.')
        KbWait;
        
        aDiff_start_vm = 22;
        aDiff_start_m = 22;
    end


%This staircases the initial ball release speed (v) parameter.
%Set up two staircases. One will start off ascending; the other
%descending. This is to prevent hysteresis and expectation

aStepsize = 5; % this has to be tested
aDiff_min = 3;
% resultData.type1.whichStair   = resultData.blindTrials+1; %use one stairCase for blind tials the other one for non-blind trials

%Generate a series of choices for the staircase and randomize them so that
%half the trials are covered by each staircase
% whichStair  = repmat([1;length(stairs)],n_trials/2,1);
% randomOrder = randperm(n_trials);
% whichStair  = whichStair(randomOrder);

%PoR
stairs_angle(1) = staircaseSetup(1, aDiff_start_vm, [aDiff_min aDiff_max], [2 1]); % vm trials
stairs_angle(2) = staircaseSetup(1, aDiff_start_m,[aDiff_min aDiff_max], [2 1]); % m task

else
    
    aStepsize = 5;
    aDiff_min = 3;
    aDiff_start_vm = 22;
    aDiff_start_m = 22;
    
    %PoR
    stairs_angle(1) = staircaseSetup(1, aDiff_start_vm, [aDiff_min aDiff_max], [2 1]); % vm trials
    stairs_angle(2) = staircaseSetup(1, aDiff_start_m,[aDiff_min aDiff_max], [2 1]); % m task
end

if startingblock==1 && strcmp(vid_or_act,'a')
    for b=1:blocks
        resultDataAngle.type1(b).whichStair   = resultDataAngle.block(b).blindTrials+1; %use one stairCase for blind trials the other one for non-blind trials
    end
    resultDataAngle.stairs = stairs_angle;
end

end

