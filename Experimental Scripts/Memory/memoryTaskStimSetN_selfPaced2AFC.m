function memoryTaskStimSetNUpDown(~)
% This experiment is a replication of the Morales et al (2018).
%
% Code (c) Polina Arbuzova (with bits from Peter Scarfe (Stroop task script) and
% Marcus Rothkirch (Reward expectation and stimulus detection under CFS'
% script)); abstract shapes scripts by Jorge Morales (as from his GitHub).

% MEMORY TASK - VARYING NUMBER OF STIMULI IN THE ENCODING PHASE

% Clear the workspace
clear all;
close all;
sca;

tic % for measuring how long it takes to do the task

%% User

user = 'polina';

switch user
    case {'polina'}
        addpath(genpath('~/Dropbox/PhDs/Polina/YoungAdults/'))
        expDir = '~/Dropbox/PhDs/Polina/YoungAdults/ExperimentalScripts/Memory/';
        saveDir = '~/Dropbox/PhDs/Polina/YoungAdults/Data/';
    case {'metamotorlab'}
        addpath(genpath('~/Dropbox/PhDs/Polina/YoungAdults/'))
        expDir = '~/Dropbox/PhDs/Polina/YoungAdults/ExperimentalScripts/Memory/';
        saveDir = '~/Dropbox/PhDs/Polina/YoungAdults/Data';
end

%%
subject = input('Subject: '); % subject nr

% Check if the subject number has been used already
while exist([saveDir filesep 'Subject' num2str(subject) filesep 'MemoryNumStim' filesep 'results_full_' num2str(subject) '.mat'],'file')
    subject = input('Participants ID already taken. Choose a different one: ');
end

debug = input('Debugging mode? (1 for debugging, 0 for full screen): ');
mode = input('Is it feedback trials (f), feedback trials with confidence ratings (c), training (t), or main part (m)?: ', 's');

if mode == 't'|| mode == 'f' || mode == 'c' || mode == 'm'
    modeChosen = 1;
else modeChosen = 0;
end

% Check that the operator chooses a meaningful input
while modeChosen ~= 1
    disp('Please choose either training (t), or training with confidence ratings (c) or main part (m) mode');
    mode = input('Is it feedback trials (f), feedback trials with confidence ratings (c), training (t), or main part (m)?: ', 's');
    if mode == 't'|| mode == 'f' || mode == 'c' || mode == 'm'
        modeChosen = 1
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%----------------------------------------------------------------------
%                       Number of trials
%----------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Setting for each mode
if mode == 't'
    runs = 5; % number of runs/blocks, default 5
    trialsPerRun = 9; % default 9
    numStim = runs * trialsPerRun;
elseif mode == 'f'
    runs = 3; % number of runs/blocks
    trialsPerRun = 5;
    numStim = runs * trialsPerRun;
elseif mode == 'c'
    runs = 2; % number of runs/blocks
    trialsPerRun = 9;
    numStim = runs * trialsPerRun;
elseif mode == 'm'
    %trialsPerRun = 8;
    cd(saveDir);
    staircase = load(['results_training_full_' num2str(subject), '.mat']);
    trialsPerRun = staircase.trialsPerRunAll(end);
    numStim = 180; % ~20 small blocks
end

%% Create folder for the participant
if ~exist([saveDir filesep 'Subject' num2str(subject) filesep 'MemoryNumStim' filesep],'dir')
    mkdir([saveDir filesep 'Subject' num2str(subject) filesep 'MemoryNumStim' filesep])
end
saveDir = [saveDir filesep 'Subject' num2str(subject) filesep 'MemoryNumStim' filesep];
stimDir = [expDir filesep 'memoryTargets' filesep];
distrDir = [expDir filesep 'memoryDistractors' filesep];
%
%%
% Setup PTB with some default values
PsychDefaultSetup(2);

% Seed the random number generator
rand('seed', sum(100*clock));

% % No VBL is implemented here. Uncomment when VBL error gets too annoying
% Screen('Preference', 'SkipSyncTests', 1);

%% General settings & variables

obs_dist = 600;         % observer distance to screen in mm
screen_width_mm = 310;  % screen width in mm
grey =[255/2 255/2 255/2]; % background colour - middle grey

if debug == 1
    screenRect = [1 1 1400 1000];
else
    screenRect = [];
end

% Appearance of the lines
thickLine = 6;
thinLine = 2;
noLine = 0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%----------------------------------------------------------------------
%                       Timings
%----------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

iti = 0.5; % in seconds,  0.5 originally
stimConfirm = 0.5;
trialsPerRunMin = 2;
trialsPerRunMax = 20;

twoAFCDisp = 2; % default 2
stimDisp = 1.5; % default 2

% Stimulus display time

% if mode == 't' || mode == 'f' || mode == 'c'
%     stimDisp = 3; % default 3
% elseif mode == 'm'
%     cd(saveDir);
%     if exist(['results_training_full_' num2str(subject)], 'file')
%         staircase = load(['results_training_full_' num2str(subject)], 'stimDisp');
%         stimDisp = staircase.stimDisp;
%     else
%         stimDisp = 2.5; % default 2, in first pilot in was 3
%     end
%
% end

% Screen
% -----------------------------------------------------------------------
% % No VBL is implemented here. Uncomment when VBL error gets too annoying
Screen('Preference', 'SkipSyncTests', 1);

InitializeMatlabOpenGL( [], [], [], 0 );


screenNumber = min(Screen('Screens'));

[win , rect] = Screen( 'OpenWindow', screenNumber, grey, screenRect, [], [], 0, 0 );

%%
Screen('FillRect',win, grey, rect); % background
[screen_width, screen_height]= Screen('WindowSize', win);	% Get screen size
[scr.centerX, scr.centerY]=RectCenter(rect);   % get center of screen
Screen('BlendFunction', win, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA'); % alpha correction of the screen
% -----------------------------------------------------------------------

% Text
% -----------------------------------------------------------------------
Screen('TextFont',win, 'Arial'); % text font
text_size = 32; % text size for instructions
if debug
    text_size = 14;
end
Screen('TextSize', win, text_size);
Screen('TextStyle', win, 0); % 1 - bold
white = [255 255 255];
% -----------------------------------------------------------------------

% Flip to clear
Screen('Flip', win);

%----------------------------------------------------------------------
%                       Keyboard information
%----------------------------------------------------------------------

% Define the keyboard keys that are listened for. We will be using the left
% and right arrow keys as response keys for the task and the escape key as
% a exit/reset key
escapeKey = KbName('ESCAPE');
upleftKey = KbName('a'); % "s" for up
downrightKey = KbName('d'); % "f" for down

buttons={'a','d', 'k', 'j', 'h', 'g'};  % define buttons that participant needs for experiment
% Note: only these buttons will be checked
% during the experiment (to save time), no other button
% presses will be recorded
kbNameResult = KbName('KeyNames');
kbNameResult(cellfun(@isempty,kbNameResult))={''};
RestrictKeysForKbCheck(find(ismember(kbNameResult,buttons)));

% For response box
devices = PsychHID('Devices'); % get all devices
device_manufacturer = {devices.manufacturer}; % find the manufacturer of them
responseBoxID = find(cellfun(@(x) strcmpi(x, 'Black Box Toolkit Ltd.'), device_manufacturer)); % get the ID number of the response box

kbNum = responseBoxID; % number of the input device

%% define stimulus properties

% Colors of stimuli
white = [0, 0, 0];
blue = [0, 0, 255];
red = [255, 0, 0];
green = [0, 255, 0];

% To draw the fixation cross, draw two bars. The dimensions are the
% following (in dva);
barwidth = 0.2;
barlength = 1;
% For the rectangles:
fixcrossver = [0 0 barwidth barlength];
fixcrosshor = [0 0 barlength barwidth];
% Define squares (and stimulus) dimension
squaresize = 5;
% Generate target screens
maxStim = 300; % number of pre-generated stimuli in the set

% Reshuffle indeces
shuffleInd = 1:1:maxStim;
shuffleInd = shuffleInd(randperm(length(shuffleInd)));
cd(stimDir);
load('stimParameters.mat');
for n=1:numStim + trialsPerRunMax
    star_stim{n} = shapes{shuffleInd(n)}.stimMatrix; % take random stimuli from the set and make them middle grey (thus /2)
end

% Generate distractor screens
clearvars p shapes shuffleInd % just in case there's something left over
shuffleInd = 1:1:maxStim;
shuffleInd = shuffleInd(randperm(length(shuffleInd)));
cd(distrDir);
load('stimParameters.mat');
for n=1:numStim + trialsPerRunMax
    distr_stim{n} = shapes{shuffleInd(n)}.stimMatrix; % take random stimuli from the set and make them middle grey (thus /2)
end

%% transfom degr of visual angle to pixel values (adapted from M. Rothkirch)

var_list_degr={'barlength'; ...  % list all variables here that have to be transformed to pixel values
    'barwidth'; ...
    'fixcrossver'; ...
    'fixcrosshor'; ...
    'squaresize'};

for var=1:length(var_list_degr)
    eval([var_list_degr{var} ' = ((tand(' num2str(var_list_degr{var}) ') .* ' ...
        num2str(obs_dist) ' .* ' num2str(screen_width) ') ./ ' ...
        num2str(screen_width_mm) ');']);
end

%% determine stimulus positions
% Define squares
square = [0 0 squaresize squaresize];
% Central position for the square
centre = OffsetRect(square, (scr.centerX)-(squaresize/2),scr.centerY-squaresize/2);
% Define square position
% % UP DOWN
% s1 = OffsetRect(square, (scr.centerX)-(squaresize/2),scr.centerY*0.5-squaresize/2);
% s2 = OffsetRect(square, scr.centerX-squaresize/2,scr.centerY*1.5-squaresize/2);
% LEFT RIGHT
s1 = OffsetRect(square, (scr.centerX*0.5)-(squaresize/2),scr.centerY-squaresize/2);
s2 = OffsetRect(square, scr.centerX*1.5-squaresize/2,scr.centerY-squaresize/2);

% Vertical bar for fixation
r1 = OffsetRect(fixcrossver, scr.centerX-(barwidth/2),scr.centerY-(barlength/2));
% Horizontal bar
r2 = OffsetRect(fixcrosshor, scr.centerX-(barlength/2),scr.centerY-(barwidth/2));
% Drawing stars
stim_side = [0.5 1.5]; % 0.5 for up and 1.5 for down

%----------------------------------------------------------------------
%                     Make a response matrix
%----------------------------------------------------------------------
respMat = nan(numStim+trialsPerRunMax,8); % 8 - number of fields (subject, run, stimulus side, type 1 response, type 1 RT, correct/or not, confidence, confidence RT.

%%

%%%%%%%%%%%%%%%%%%%%%%
%% START EXPERIMENT %%
%%%%%%%%%%%%%%%%%%%%%%
% For transportability between Mac and Windows
KbName('UnifyKeyNames');

if ~debug
    HideCursor();
end

% Display welcome screen
% welcomeMessage =' Welcome to the experiment! \n\nIf you have any
% questions about the procedure please ask the experimenter. \n\n\nPlease
% press any button to begin. \n\n\n'; % in English
welcomeMessage = 'Willkommen zum Experiment! \n\nWenn Sie Fragen zum Verfahren haben, wenden Sie sich bitte an den Experimentator. \n\n\nBitte druecken Sie eine beliebige Taste, um zu beginnen.';
Screen('TextSize', win, text_size);
DrawFormattedText(win, welcomeMessage, scr.centerX*0.5, scr.centerY);
% Present fixation crosses and placeholders
Screen('Flip',win);
WaitSecs(.5);
KbWait(kbNum);

n=1;
r = 1;
lastRun = 0;
while n <= numStim + trialsPerRunMax %
    %for r=1:runs
    % Start the experimental loop
    
    % Save the current stimulus display time
    
    Screen('TextSize', win, text_size);
    %memoPhaseText = 'Memorization phase. Please press any key to start.
    %\n'; % in English
    memoPhaseText = 'Lernphase. Bitte druecken Sie eine beliebige Taste, um zu starten.';
    DrawFormattedText(win, memoPhaseText, 'center', scr.centerY);
    Screen('Flip', win);
    WaitSecs(iti);
    KbWait(kbNum);
    
    % Show the stimuli for memorization
    if r == 1
        firstTrialRun = 1;
        lastTrialRun = trialsPerRun;
    elseif r>1
        firstTrialRun = sum(trialsPerRunAll) + 1;
        lastTrialRun = sum(trialsPerRunAll) + trialsPerRun;
    end
    
    
    for n=firstTrialRun:lastTrialRun % probably wrong, need to randomize
        % Draw a rectangle contour at
        % location s1/2, with a line width of 1 pixel:
        %
        Screen('FrameRect', win, blue, centre, thinLine);
        % Present fixation crosses and placeholders
        Screen('Flip', win);
        WaitSecs(iti);
        target = Screen('MakeTexture', win,star_stim{n});
        Screen('DrawTextures', win, target, [], centre);
        Screen('FrameRect', win, blue, centre, thinLine);
        Screen('Flip', win);
        WaitSecs(stimDisp);
    end
    
    % Recall phase
    Screen('TextSize', win, text_size); % for the goodbye screen
    %recallTextPhase = 'Recall phase. Please press any key to start. \n'; %
    %in English
    recallPhaseText = 'Erinnerungsphase. Bitte druecken Sie eine beliebige Taste, um zu starten.';
    DrawFormattedText(win, recallPhaseText, 'center', scr.centerY);
    Screen('Flip', win);    
    WaitSecs(0.2);
    KbWait(kbNum);

    
    % Define target side (1 - up, 2 - down)
    stim_side = randi([1 2],numStim + trialsPerRunMax, 1)';
    
    % Randomize the order of the targets displayed
    runInd = firstTrialRun:1:lastTrialRun;
    runInd = runInd(randperm(length(runInd)));
    
    % Show the targets from memorization and distractor stimuli
    
    for n=firstTrialRun:lastTrialRun
        idx = rem(n,trialsPerRun);
        if idx == 0
            idx = trialsPerRun;
        end
        target = Screen('MakeTexture',win,star_stim{runInd(idx)});
        distractor = Screen('MakeTexture',win, distr_stim{runInd(idx)});
        
        if stim_side(n) == 1
            Screen('DrawTextures',win, target, [], s1);
            Screen('DrawTextures',win, distractor, [], s2);
        elseif stim_side(n)== 2
            Screen('DrawTextures',win, target, [], s2);
            Screen('DrawTextures',win, distractor, [], s1);
        end
        % Draw a rectangle contour at
        % location s1/2, with a line width of 1 pixel:
        % Up
        Screen('FrameRect',win, blue, s1, thinLine);
        % Down
        Screen('FrameRect',win, blue, s2, thinLine);
        
        taskQuestion = 'Welches Bild haben Sie schon gesehen, \nLINKS oder RECHTS? ';
        Screen('TextStyle', win, 0); % 1 for bold
        DrawFormattedText(win, taskQuestion, 'center', scr.centerY, white);
        
        
        Screen('Flip', win);
        tStart = GetSecs;
        WaitSecs(0.2);
        
        % Cue to determine whether a response has been made
        timedout = false;
        respToBeMade = true;
        while ~timedout
            % check if a key is pressed
            % only keys specified in activeKeys are considered valid
            [ keyIsDown, secs, keyCode ] = KbCheck;
            if(keyIsDown), break; end
            if( (secs - tStart) > twoAFCDisp)
                timedout = true; %end
                if keyCode(escapeKey)
                    ShowCursor;
                    sca;
                    return
                elseif keyCode(upleftKey) % participant chose up
                    response = 1;
                    respToBeMade = false;
                    % Draw a rectangle contour at
                    % location s1/2, with a thicker line on the chosen side:
                    % Up
                    Screen('FrameRect', win, blue, s1, thickLine);
                    % Down
                    Screen('FrameRect', win, blue, s2, thinLine);
                    
                    % For the feedback trials
                    if mode == 'f'
                        if stim_side(n) == response % correct trial
                            % Up
                            Screen('FrameRect', win, green, s1, thickLine);
                            % Down
                            Screen('FrameRect', win, blue, s2, thinLine);
                        else % incorrect trial
                            % Up
                            Screen('FrameRect', win, red, s1, thickLine);
                            % Down
                            Screen('FrameRect', win, blue, s2, thinLine);
                        end
                    end
                    
                elseif keyCode(downrightKey) % participant chose right
                    response = 2;
                    respToBeMade = false;
                    % Draw a rectangle contour at
                    % location s1/2, with a thicker line on the chosen side:
                    % Up
                    Screen('FrameRect', win, blue, s1, thinLine);
                    % Down
                    Screen('FrameRect', win, blue, s2, thickLine);
                    
                    % For the feedback trials
                    if mode == 'f'
                        if stim_side(n) == response % correct trial
                            % Up
                            Screen('FrameRect', win, blue, s1, thinLine);
                            % Down
                            Screen('FrameRect', win, green, s2, thickLine);
                        else % incorrect trial
                            % Up
                            Screen('FrameRect', win, blue, s1, thinLine);
                            % Down
                            Screen('FrameRect', win, red, s2, thickLine);
                        end
                    end
                end
            end
        end
        while respToBeMade == true
            
            %tStart = GetSecs;
            % Record the response
            % Show the squares
            % Draw a rectangle contour at
            % location s1/2, with a line width of 1 pixel:
            % Up
            Screen('FrameRect',win, blue, s1, thinLine);
            % Down
            Screen('FrameRect',win, blue, s2, thinLine);
            % Present fixation crosses and placeholders
            % Write the question
            taskQuestion = 'Welches Bild haben Sie schon gesehen, \nLINKS oder RECHTS? ';
            Screen('TextStyle', win, 0); % 1 for bold
            DrawFormattedText(win, taskQuestion, 'center', scr.centerY, white);
            
            Screen('Flip',win);
            
            
            
            
            % Check the keyboard
            [keyIsDown, secs, keyCode] = KbCheck(kbNum);
            if keyCode(escapeKey)
                ShowCursor;
                sca;
                return
            elseif keyCode(upleftKey) % participant chose up
                response = 1;
                respToBeMade = false;
                % Draw a rectangle contour at
                % location s1/2, with a thicker line on the chosen side:
                % Up
                Screen('FrameRect', win, blue, s1, thickLine);
                % Down
                Screen('FrameRect', win, blue, s2, thinLine);
                
                % For the feedback trials
                if mode == 'f'
                    if stim_side(n) == response % correct trial
                        % Up
                        Screen('FrameRect', win, green, s1, thickLine);
                        % Down
                        Screen('FrameRect', win, blue, s2, thinLine);
                    else % incorrect trial
                        % Up
                        Screen('FrameRect', win, red, s1, thickLine);
                        % Down
                        Screen('FrameRect', win, blue, s2, thinLine);
                    end
                end
                
            elseif keyCode(downrightKey) % participant chose right
                response = 2;
                respToBeMade = false;
                % Draw a rectangle contour at
                % location s1/2, with a thicker line on the chosen side:
                % Up
                Screen('FrameRect', win, blue, s1, thinLine);
                % Down
                Screen('FrameRect', win, blue, s2, thickLine);
                
                % For the feedback trials
                if mode == 'f'
                    if stim_side(n) == response % correct trial
                        % Up
                        Screen('FrameRect', win, blue, s1, thinLine);
                        % Down
                        Screen('FrameRect', win, green, s2, thickLine);
                    else % incorrect trial
                        % Up
                        Screen('FrameRect', win, blue, s1, thinLine);
                        % Down
                        Screen('FrameRect', win, red, s2, thickLine);
                    end
                end
            end
        end
        
        
        Screen('TextStyle', win, 0); % 1 for bold
        DrawFormattedText(win, taskQuestion, 'center', scr.centerY, white);
        
        % Determine type 1 RT
        tEnd = GetSecs;
        rt = tEnd - tStart;
        
        Screen('Flip', win);
        WaitSecs(stimConfirm);
        
        % Record the trial data into out data matrix
        respMat(n,1) = subject; % subject index
        respMat(n,2) = r; % run index
        respMat(n,3) = stim_side(n); % cue_type;
        respMat(n,4) = response; % response
        respMat(n,5) = rt; % rt in s
        
        % Determine response accuracy
        if respMat(n,3)==respMat(n,4) % response accuracy
            respCorr = 1; % for confidence ratings with feedback
            respMat(n,6)=1;
        else
            respMat(n,6)=0;
            respCorr = 0;
        end
        
        
        % Type 2 response
        if mode == 'm' || mode == 'c'
            cd(expDir);
            [confidence, RT_conf] = ConfidenceScaleDiscreteLines(win, scr, respCorr, mode);
        elseif mode == 't' || mode == 'f'
            confidence = NaN;
            RT_conf    = NaN;
        end
        
        % Record the trial data for type 2 response into the data matrix
        respMat(n,7) = confidence;
        respMat(n,8) = RT_conf;
        
        
        trialsPerRunAll(r) = trialsPerRun; % save the value in each run
    end
    
    % Adjusting difficulty
    
    % Check if performance level is within the desired range (between 60%
    % and 80%), across two last mini-blocks (or the first one only for the
    % first block)
    if r == 1
        percCorr = nanmean(respMat(firstTrialRun:lastTrialRun,6));
    else
        prevFirstTrialRun = firstTrialRun - trialsPerRunAll(r-1);
        percCorr = nanmean(respMat(prevFirstTrialRun:lastTrialRun,6));
    end
    
    if percCorr < 0.60
        trialsPerRun = trialsPerRun - 1;
        disp(['Proportion Correct: ', num2str(percCorr), ' Difficulty will decrease.' ])
    elseif percCorr > 0.80
        trialsPerRun = trialsPerRun + 1;
        disp(['Proportion Correct: ', num2str(percCorr), ' Difficulty will increase.' ])
    else
        disp(['Proportion Correct: ', num2str(percCorr), ' Difficulty will not change.' ])
    end
    
    % Check that it's not too low or not too high
    if trialsPerRun < trialsPerRunMin
        trialsPerRun = trialsPerRunMin;
    elseif trialsPerRun > trialsPerRunMax
        trialsPerRun = trialsPerRunMax;
    end
    
    
    %% Save data
    cd(saveDir);
    if mode == 'm' % for the main part
        save(['results_' num2str(subject), '.mat'],'respMat','-v7.3');
        save(['results_full_' num2str(subject), '.mat'],'-v7.3'); % full file just in case
    elseif mode == 't' % for the training (staircasing) part
        save(['results_training_' num2str(subject), '.mat'],'respMat','-v7.3');
        save(['results_training_full_' num2str(subject), '.mat'],'-v7.3'); % full file just in case
    elseif mode == 'f' % for the training (staircasing) part
        save(['results_feedback_' num2str(subject), '.mat'],'respMat','-v7.3');
        save(['results_feedback_full_' num2str(subject), '.mat'],'-v7.3'); % full file just in case
    elseif mode == 'c'
        save(['results_confidence_' num2str(subject), '.mat'],'respMat','-v7.3');
        save(['results_confidence_full_' num2str(subject), '.mat'],'-v7.3'); % full file just in case
    end
    
    % Check if there are enough trials done
    
    % If it's the last run and the last trial, break
    if lastRun == 1 && n == lastTrialRun
        disp('Case 1');
        break;
    end
    
    
    if lastTrialRun == numStim
        disp('Case 2');
        break;
    end
    
    % Check if it's the last run
    if lastRun ~= 1
        if (numStim-n) < trialsPerRun && lastTrialRun ~= numStim
            lastRun = 1;
        end
    end
    
    r = r+1; % increase the number of runs
    
    % Break screen
    %breakMessage = 'num2str(n) ' trials out of ' num2str(numStim) '
    %completed. \n Please press any key to continue. \n\n '; % in English
    breakMessage=[num2str(n) ' Versuche von ' num2str(numStim) ' abgeschlossen. \n Bitte druecken Sie eine Taste um fortzufahren.'];
    
    Screen('TextSize', win, text_size); % for the goodbye screen
    DrawFormattedText(win, breakMessage, scr.centerX*0.5, scr.centerY);
    Screen('Flip', win);
    WaitSecs(.7);
    KbWait(kbNum);
    
    
end % end of while loop
% Remove unnecessary NaNs from the response matrix
spareTrials = find(isnan(respMat(:,1))); % find the first raw with no subject number - must be the first raw with empty trials
respMat = respMat(1:spareTrials(1)-1,:); % save the response matrix until then

%% Save data
cd(saveDir);
if mode == 'm' % for the main part
    save(['results_' num2str(subject), '.mat'],'respMat','-v7.3');
    save(['results_full_' num2str(subject), '.mat'],'-v7.3'); % full file just in case
end


Screen('Flip', win);
Screen('TextSize', win, text_size); % for the goodbye screen
%endMessage = 'Thank you for taking part! Press any button to quit. \n '; %
%in English
endMessage = 'Vielen Dank fuer Ihre Teilnahme! Druecken Sie eine beliebige Taste, um den Vorgang zu beenden.';
DrawFormattedText(win, endMessage, 'center', scr.centerY, white);
Screen('Flip', win);
WaitSecs(.7);
% Press any button to finish the experiment
KbWait(kbNum);

Screen('CloseAll');
sca;

% print how lown it took to do the experiment
time = toc/60; % in minutes
disp('');
disp(['This part of the experiment took: ', num2str(time), ' minutes.']);

end
