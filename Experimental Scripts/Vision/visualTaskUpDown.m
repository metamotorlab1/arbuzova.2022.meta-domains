function visualTaskUpDown(~)
% This experiment is a replication of the Morales et al (2018).
%
% Code (c) Polina Arbuzova (with bits from Peter Scarfe (Stroop task script) and
% Marcus Rothkirch (Reward expectation and stimulus detection under CFS'
% script)); abstract shapes scripts by Jorge Morales (as from his GitHub).

% VISUAL TASK

clear all;
close all;
sca;
tic % for measuring how long it takes to do the task
%% User

user = 'polina';

switch user
    case {'polina'}
        addpath(genpath('~/Dropbox/PhDs/Polina/YoungAdults/'))
        expDir = '~/Dropbox/PhDs/Polina/YoungAdults/ExperimentalScripts/Vision/';
        saveDir = '~/Dropbox/PhDs/Polina/YoungAdults/Data';
    case {'metamotorlab'}
        addpath(genpath('~/Dropbox/PhDs/Polina/YoungAdults/'))
        expDir = '~/Dropbox/PhDs/Polina/YoungAdults/ExperimentalScripts/Vision/';
        saveDir = '~/Dropbox/PhDs/Polina/YoungAdults/Data';
end
%%

subject = input('Subject: '); % subject nr
% Check if the subject number has been used already
while exist([saveDir filesep 'Subject' num2str(subject) filesep 'Visual' filesep 'results_full_' num2str(subject) '.mat'],'file')
    subject = input('Participants ID already taken. Choose a different one: ');
end

debug = input('Debugging mode? (1 for debugging, 0 for full screen): ');
mode = input('Is it feedback trials (f), feedback trials with confidence ratings (c), training (t), or main part (m)?: ', 's');
if mode == 't'|| mode == 'f' || mode == 'c' || mode == 'm'
    modeChosen = 1;
else modeChosen = 0;
end
% Check that the operator chooses a meaningful input
while modeChosen ~= 1
    disp('Please choose either training (t), or training with confidence ratings (c) or main part (m) mode');
    mode = input('Is it feedback trials (f), feedback trials with confidence ratings (c), training (t), or main part (m)?: ', 's');
    if mode == 't'|| mode == 'f' || mode == 'c' || mode == 'm'
        modeChosen = 1
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%----------------------------------------------------------------------
%                       Number of trials
%----------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Setting for each mode
if mode == 't'
    runs = 1; % number of runs/blocks
    trialsPerRun = 40; % default 40
elseif mode == 'f'
    runs = 1; % number of runs/blocks
    trialsPerRun = 12;
elseif mode == 'c'
    runs = 1; % number of runs/blocks
    trialsPerRun = 12; % default 12
elseif mode == 'm'
    runs = 6; % default 5; number of runs/blocks
    trialsPerRun = 30; % default 40 for 200 trials overall
end

numStim = runs * trialsPerRun;


%% Create folder for the participant
if ~exist([saveDir filesep 'Subject' num2str(subject) filesep 'Visual' filesep],'dir')
    mkdir([saveDir filesep 'Subject' num2str(subject) filesep 'Visual' filesep])
end
saveDir = [saveDir filesep 'Subject' num2str(subject) filesep 'Visual' filesep];
stimDir = [expDir filesep 'visualTargets' filesep];
distrDir = [expDir filesep 'visualDistractors' filesep];
%
%%
% Setup PTB with some default values
PsychDefaultSetup(2);

% Seed the random number generator
rand('seed', sum(100*clock));

% % No VBL is implemented here. Uncomment when VBL error gets too annoying
% Screen('Preference', 'SkipSyncTests', 1);

%% General settings & variables

obs_dist = 600;         % observer distance to screen in mm
screen_width_mm = 310;  % screen width in mm
grey =[255/2 255/2 255/2]; % background colour - middle grey

if debug == 1
    screenRect = [1 1 900 650];
else
    screenRect = [];
end

% Screen
% -----------------------------------------------------------------------
% % No VBL is implemented here. Uncomment when VBL error gets too annoying
Screen('Preference', 'SkipSyncTests', 1);

InitializeMatlabOpenGL( [], [], [], 0 );


screenNumber = min(Screen('Screens'));

[win , rect] = Screen( 'OpenWindow', screenNumber, grey, screenRect, [], [], 0, 0 );

%%
Screen('FillRect',win, grey, rect); % background
[screen_width, screen_height]= Screen('WindowSize', win);	% Get screen size
[scr.centerX, scr.centerY]=RectCenter(rect);   % get center of screen
Screen('BlendFunction', win, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA'); % alpha correction of the screen
% -----------------------------------------------------------------------

% Text
% -----------------------------------------------------------------------
Screen('TextFont',win, 'Arial'); % text font
text_size = 32; % text size for instructions
if debug
    text_size = 14;
end
Screen('TextSize', win, text_size);
Screen('TextStyle', win, 0); % 1 - bold
white = [255 255 255];
% -----------------------------------------------------------------------

% Flip to clear
Screen('Flip', win);

%----------------------------------------------------------------------
%                       Keyboard information
%----------------------------------------------------------------------

% Define the keyboard keys that are listened for. We will be using the left
% and right arrow keys as response keys for the task and the escape key as
% a exit/reset key
escapeKey = KbName('ESCAPE');
upleftKey = KbName('a'); % "s" for up
downrightKey = KbName('d'); % "f" for down

buttons={'a','d', 'k', 'j', 'h', 'g'};  % define buttons that participant needs for experiment
% Note: only these buttons will be checked
% during the experiment (to save time), no other button
% presses will be recorded
kbNameResult = KbName('KeyNames');
kbNameResult(cellfun(@isempty,kbNameResult))={''};
RestrictKeysForKbCheck(find(ismember(kbNameResult,buttons)));

% For response box
devices = PsychHID('Devices'); % get all devices
device_manufacturer = {devices.manufacturer}; % find the manufacturer of them
responseBoxID = find(cellfun(@(x) strcmpi(x, 'Black Box Toolkit Ltd.'), device_manufacturer)); % get the ID number of the response box

kbNum = responseBoxID; % number of the input device

%% define stimulus properties
% Colors of stimuli
white = [0, 0, 0];
blue = [0, 0, 255];
red = [255, 0, 0];
green = [0, 255, 0];

% To draw the fixation cross, draw two bars. The dimensions are the
% following (in dva);
barwidth = 0.2;
barlength = 1;
% For the rectangles:
fixcrossver = [0 0 barwidth barlength];
fixcrosshor = [0 0 barlength barwidth];
% Define squares (and stimulus) dimension
squaresize = 5;
% Generate target screens
maxStim = 300; % number of pre-generated stimuli in the set

% Reshuffle indeces
shuffleInd = 1:1:maxStim;
shuffleInd = shuffleInd(randperm(length(shuffleInd)));
cd(stimDir);
load('stimParameters.mat');
for n=1:numStim
    star_stim{n} = shapes{shuffleInd(n)}.stimMatrix; % take random stimuli from the set and make them middle grey (thus /2)
end

% Generate distractor screens
clearvars p shapes shuffleInd % just in case there's something left over
shuffleInd = 1:1:maxStim;
shuffleInd = shuffleInd(randperm(length(shuffleInd)));
cd(distrDir);
load('stimParameters.mat');
for n=1:numStim
    distr_stim{n} = shapes{shuffleInd(n)}.stimMatrix; % take random stimuli from the set and make them middle grey (thus /2)
end

%% transfom degr of visual angle to pixel values (adapted from M. Rothkirch)

var_list_degr={'barlength'; ...  % list all variables here that have to be transformed to pixel values
    'barwidth'; ...
    'fixcrossver'; ...
    'fixcrosshor'; ...
    'squaresize'};

for var=1:length(var_list_degr)
    eval([var_list_degr{var} ' = ((tand(' num2str(var_list_degr{var}) ') .* ' ...
        num2str(obs_dist) ' .* ' num2str(screen_width) ') ./ ' ...
        num2str(screen_width_mm) ');']);
end

%% determine stimulus positions
% Define squares
square = [0 0 squaresize squaresize];
% Fixation cross
% Vertical bar
r1 = OffsetRect(fixcrossver, scr.centerX-(barwidth/2),scr.centerY-(barlength/2));
% Horizontal bar
r2 = OffsetRect(fixcrosshor, scr.centerX-(barlength/2),scr.centerY-(barwidth/2));
% Central position for the square
centre = OffsetRect(square, (scr.centerX)-(squaresize/2),scr.centerY-squaresize/2);
% Define square position
% % UP DOWN
% s1 = OffsetRect(square, (scr.centerX)-(squaresize/2),scr.centerY*0.5-squaresize/2);
% s2 = OffsetRect(square, scr.centerX-squaresize/2,scr.centerY*1.5-squaresize/2);
% LEFT RIGHT
s1 = OffsetRect(square, (scr.centerX*0.5)-(squaresize/2),scr.centerY-squaresize/2);
s2 = OffsetRect(square, scr.centerX*1.5-squaresize/2,scr.centerY-squaresize/2);

% Vertical bar for fixation
r1 = OffsetRect(fixcrossver, scr.centerX-(barwidth/2),scr.centerY-(barlength/2));
% Horizontal bar
r2 = OffsetRect(fixcrosshor, scr.centerX-(barlength/2),scr.centerY-(barwidth/2));
% Drawing stars
stim_side = [0.5 1.5]; % 0.5 for left and 1.5 for right

% Appearance of the lines
thickLine = 6;
thinLine = 2;
noLine = 0;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%----------------------------------------------------------------------
%                       Timings
%----------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
iti = 0.5;
stim_present = 0.1;
feedback_present = 0.5;


%%

%----------------------------------------------------------------------
%                     Make a response matrix
%----------------------------------------------------------------------
respMat = nan(numStim,8); % 8 - number of fields (subject, run, stimulus side, type 1 response, type 1 RT, correct/or not, confidence, confidence RT.

%% Staircasing
bDiff.min = 1;
bDiff.max = 125; % let's say 125
bDiff.step = 5;

if mode == 't' || mode == 'f' || mode == 'c'
    bDiff.start = 30; % starting value for the staircase
    bDiff.staircase = staircaseSetup(1, bDiff.start, [bDiff.min bDiff.max], [2 1]);
    bDiff.staircaseAll = [];
elseif mode == 'm'
    cd(saveDir);
    staircase = load(['results_training_full_' num2str(subject), '.mat']);
    bDiff.start = staircase.bDiff.staircaseAll(end);
    bDiff.staircase = staircaseSetup(1, bDiff.start, [bDiff.min bDiff.max], [2 1]);
    bDiff.staircaseAll = [];
end
%%
%%%%%%%%%%%%%%%%%%%%%%
%% START EXPERIMENT %%
%%%%%%%%%%%%%%%%%%%%%%
% For transportability between Mac and Windows
KbName('UnifyKeyNames');

if ~debug
    HideCursor();
end

% Display welcome screen
%welcomeMessage=' Welcome to the experiment! \n\n If you have any questions
%about the procedure please ask the experimenter. \n\n\n Please press any
%button to begin. \n\n\n '; % in English
welcomeMessage = 'Willkommen zum Experiment! \n\n Wenn Sie Fragen zum Verfahren haben, wenden Sie sich bitte an den Experimentator. \n\n\n Bitte druecken Sie eine beliebige Taste, um zu beginnen.';
Screen('TextSize', win, text_size);
DrawFormattedText(win, welcomeMessage, 'center', scr.centerY);
Screen('Flip',win);
WaitSecs(.7);
KbWait(kbNum);

for r=1:runs
    % Start the experimental loop
    
    % Comparison
    Screen('TextSize', win, text_size);
    %startMessage=' Visual Perception Experiment. \n\n  You will need to
    %tell which picture - left or right - was BRIGHTER. \n\n\n  Please
    %press any key to start. \n\n\n  '; % in English
    startMessage = 'Experiment zur visuellen Wahrnehmung. \n\n  Sie muessen feststellen, welches Bild (links oder rechts) HELLER war. \n\n\n  Bitte druecken Sie eine beliebige Taste, um zu starten.';
    DrawFormattedText(win, startMessage, 'center', scr.centerY);
    Screen('Flip', win);
    
    WaitSecs(.7);
    KbWait(kbNum);
    
    % Draw the starting scene
    WaitSecs(iti);
    
    %%
    
    % Define target side (1 - left, 2 - right)
    stim_side = randi([1 2],numStim,1)';
    
    % Show the 9 target and 9 distractor stimuli
    for n=(r*trialsPerRun)-(trialsPerRun-1):r*trialsPerRun
        
        % Draw a rectangle contour at
        % location s1/2, with a line width of 1 pixel:
        % Left
        Screen('FrameRect', win, blue, s1, thinLine);
        % Right
        Screen('FrameRect', win, blue, s2, thinLine);
        
        % Draw fixation crosses (draw a rectangle contour at location r2, with a line width of 1 pixel:)
        Screen('FillRect',win, white, r1, noLine);
        Screen('FillRect',win, white, r2, noLine);
        
        Screen('Flip', win);
        WaitSecs(iti);
        
        distr_stim{n} = distr_stim{n}-bDiff.staircase.Signal;
        target = Screen('MakeTexture',win,star_stim{n});
        distractor = Screen('MakeTexture',win, distr_stim{n});
        if stim_side(n) == 1
            Screen('DrawTextures', win, target, [], s1);
            Screen('DrawTextures', win, distractor, [], s2);
        elseif stim_side(n)== 2
            Screen('DrawTextures', win, target, [], s2);
            Screen('DrawTextures', win, distractor, [], s1);
        end
        % Draw fixation crosses (draw a rectangle contour at location r2, with a line width of 1 pixel:)
        Screen('FillRect',win, white, r1, noLine);
        Screen('FillRect',win, white, r2, noLine);
        
        % Draw a rectangle contour at
        % location s1/2, with a line width of 1 pixel:
        % Left
        Screen('FrameRect', win, blue, s1, thinLine);
        % Right
        Screen('FrameRect', win, blue, s2, thinLine);
        
        Screen('Flip', win);
        WaitSecs(stim_present);
        % Show the squares
        % Draw a rectangle contour at
        % location s1/2, with a line width of 2 pixel:
        % Left
        Screen('FrameRect', win, blue, s1, thinLine);
        % Right
        Screen('FrameRect', win, blue, s2, thinLine);
        % Present fixation crosses and placeholders
        % Draw fixation crosses (draw a rectangle contour at location r2, with a line width of 1 pixel:)
        %Screen('FillRect',win, white, r1, 1);
        %Screen('FillRect',win, white, r2, 1);
    
        % Write the question
        taskQuestion = 'Welches Bild hatte HELLERE Linien, \nLINKS oder RECHTS? ';
        Screen('TextStyle', win, 0);
        DrawFormattedText(win, taskQuestion, 'center', scr.centerY, white); 
        
        Screen('Flip', win);
        tStart = GetSecs;
        % Record the response
        % Cue to determine whether a response has been made
        respToBeMade = true;
        while respToBeMade == true
            % Check the keyboard
            [keyIsDown,secs, keyCode] = KbCheck(kbNum);
            if keyCode(escapeKey)
                ShowCursor;
                sca;
                return
            elseif keyCode(upleftKey) % participant chose up
                response = 1;
                respToBeMade = false;
                % Draw a rectangle contour at
                % location s1/2, with a thicker line on the chosen side:
                % Left
                Screen('FrameRect', win, blue, s1, thickLine); % 
                % Right
                Screen('FrameRect', win, blue, s2, thinLine); % 
                
                % For the feedback trials
                if mode == 'f'
                    if stim_side(n) == response % correct trial
                        % Left
                        Screen('FrameRect', win, green, s1, thickLine); % 
                        % Right
                        Screen('FrameRect', win, blue, s2, thinLine); % 
                    else % incorrect trial
                        % Left
                        Screen('FrameRect', win, red, s1, thickLine); % 
                        % Right
                        Screen('FrameRect', win, blue, s2, thinLine); % 
                    end
                end
            elseif keyCode(downrightKey) % participant chose down
                response = 2;
                respToBeMade = false;
                % Draw a rectangle contour at
                % location s1/2, with a thicker line on the chosen side:
                % Left
                Screen('FrameRect', win, blue, s1, thinLine); % 
                % Right
                Screen('FrameRect', win, blue, s2, thickLine); % 
                
                % For the feedback trials
                if mode == 'f'
                    if stim_side(n) == response % correct trial
                        % Right
                        Screen('FrameRect', win, green, s2, thickLine); % 
                        % Left
                        Screen('FrameRect', win, blue, s1, thinLine); % 
                        
                    else % incorrect trial
                        
                        % Right
                        Screen('FrameRect', win, red, s2, thickLine); % 
                        % Left
                        Screen('FrameRect', win, blue, s1, thinLine); % 
                        
                    end
                end
            end
            
            %             % Draw fixation crosses (draw a rectangle contour at location r2, with a line width of 1 pixel:)
            %             Screen('FillRect',win, white, r1, 1);
            %             Screen('FillRect',win, white, r2, 1);
            
        end
        Screen('FrameRect', win, grey, [0 1 0 1], thinLine);
        Screen('TextStyle', win, 0); % 1 for bold
        DrawFormattedText(win, taskQuestion, 'center', scr.centerY, white);
        
        
        % Determine type 1 RT
        tEnd = GetSecs;
        rt = tEnd - tStart;
        
        Screen('Flip', win);
        WaitSecs(feedback_present);
        
        
        % Record the trial data into the data matrix
        respMat(n,1) = subject; % subject index
        respMat(n,2) = r; % run index
        respMat(n,3) = stim_side(n); % cue_type;
        respMat(n,4) = response; % response
        respMat(n,5) = rt; % rt in s
        
        % Determine response accuracy
        if respMat(n,3)==respMat(n,4) % response accuracy
            respCorr = 1; % for confidence ratings with feedback
            respMat(n,6)=1;
        else
            respMat(n,6)=0;
            respCorr = 0;
        end
        
        
        % Type 2 response
        if mode == 'm' || mode == 'c'
            cd(expDir);
            [confidence, RT_conf] = ConfidenceScaleDiscreteLines(win, scr, respCorr, mode);
        elseif mode == 't' || mode == 'f'
            confidence = NaN;
            RT_conf    = NaN;
        end
        
        % Record the trial data for type 2 response into the data matrix
        respMat(n,7) = confidence;
        respMat(n,8) = RT_conf;
        
        % Staircasing
        bDiff.staircase = staircaseTrial(1, bDiff.staircase, respMat(n,6));
        bDiff.staircase = staircaseUpdate(1, bDiff.staircase, -bDiff.step);
        bDiff.staircaseAll = [bDiff.staircaseAll bDiff.staircase.Signal];
    end
    
    %% Save data
    cd(saveDir);
    if mode == 'm' % for the main part
        save(['results_' num2str(subject), '.mat'],'respMat','-v7.3');
        save(['results_full_' num2str(subject), '.mat'],'-v7.3'); % full file just in case
    elseif mode == 't' % for the training (staircasing) part
        save(['results_training_' num2str(subject), '.mat'],'respMat','-v7.3');
        save(['results_training_full_' num2str(subject), '.mat'],'-v7.3'); % full file just in case
    elseif mode == 'f' % for the training (staircasing) part
        save(['results_feedback_' num2str(subject), '.mat'],'respMat','-v7.3');
        save(['results_feedback_full_' num2str(subject), '.mat'],'-v7.3'); % full file just in case
    elseif mode == 'c'
        save(['results_confidence_' num2str(subject), '.mat'],'respMat','-v7.3');
        save(['results_confidence_full_' num2str(subject), '.mat'],'-v7.3'); % full file just in case
    end
    
    % Break screen
    if runs>1
        %breakMessage='' Run ' num2str(r) ' out of ' num2str(runs) '
        %completed. \n Please press any key to continue. \n '; % in English
        breakMessage=['Block ' num2str(r) ' von ' num2str(runs) ' abgeschlossen. \nBitte druecken Sie eine Taste um fortzufahren.'];
    else
        %breakMessage=' This part of the experiment is completed. \n Please
        %press any key to continue. \n '; % in English
        breakMessage=['Dieses Teil vom Experiment ist abgeschlossen. \nBitte druecken Sie eine Taste um fortzufahren.'];
    end
    Screen('TextSize', win, text_size); % for the break screen
    DrawFormattedText(win, breakMessage, scr.centerX*0.5, scr.centerY, white);
    Screen('Flip', win);
    WaitSecs(.7);
    KbWait(kbNum);
    
    %     % Print percentage correct per block
    %     percCorrBlock = sum(respMat((r*trialsPerRun)-(trialsPerRun-1):r*trialsPerRun,6))/length(respMat(:,6))
    
end % end of runs loop

if mode == 'm'
    Screen('Flip', win);
    Screen('TextSize', win, text_size); % for the goodbye screen
    %endMessage='Thank you for taking part! Press any button to quit. \n\n '; % in English
    endMessage = 'Vielen Dank fuer Ihre Teilnahme! Druecken Sie eine beliebige Taste, um den Vorgang zu beenden.';
    DrawFormattedText(win, endMessage, text_size,  'center' , scr.centerY);    Screen('Flip', win);
    WaitSecs(.7);
    % Press any button to finish the experiment
    KbWait(kbNum);
end

sca;
ShowCursor();

% print how lown it took to do the experiment
time = toc/60; % in minutes
disp('');
disp(['This part of the experiment took: ', num2str(time), ' minutes.']);

% show the staircase, with percentage correct in the title
percCorr = sum(respMat(:,6))/length(respMat(:,6));
figure('Name', ['Brightness: difference. Percentage correct: ', num2str(percCorr)])
plot(bDiff.staircaseAll, ['r' '-o'])
xlabel('trial')
ylabel('brightness difference')


end
