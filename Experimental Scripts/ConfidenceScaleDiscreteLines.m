function [conf, RT_conf]=ConfidenceScaleDiscreteLines(window, p, respCorr, mode)

curWindow = window;
center = [p.centerX, p.centerY];
responseKeys = [KbName('space') KbName('k') KbName('j') KbName('h') KbName('g') ];

% For response box
devices = PsychHID('Devices'); % get all devices
device_manufacturer = {devices.manufacturer}; % find the manufacturer of them
responseBoxID = find(cellfun(@(x) strcmpi(x, 'Black Box Toolkit Ltd.'), device_manufacturer)); % get the ID number of the response box

kbNum = responseBoxID; % number of the input device

p.stim.VASwidth_inPixels = 600;
p.stim.VASheight_inPixels = 20;
p.stim.VASoffset_inPixels = 100;
p.stim.arrowWidth_inPixels = 20;

p.times.confDuration_inSecs = 10;
p.times.confFBDuration_inSecs = 0.5;

white = [255 255 255];
green = [0 255 0];
red = [255 0 0];

% How many steps do you have in the discrete confidence scale 
n_steps = 4; 

%% Initialise VAS scale
VASwidth=p.stim.VASwidth_inPixels;
VASheight=p.stim.VASheight_inPixels;
VASoffset=p.stim.VASoffset_inPixels;
arrowwidth=p.stim.arrowWidth_inPixels;
arrowheight=arrowwidth*2;
l = VASwidth/2;
deadline = 0;

% Collect rating
start_time = GetSecs;
steps_x = linspace(-l, l, n_steps);

tickLabelsWords = {'sehr unsicher','unsicher','sicher','sehr sicher'};

% For version with no time pressure
keyCode=1;
while ~(find(keyCode)==responseKeys(1) || find(keyCode)==responseKeys(2) || find(keyCode)==responseKeys(3) || find(keyCode)==responseKeys(4) || find(keyCode)==responseKeys(5))
    % Draw line
    Screen('DrawLine',curWindow,white,center(1)-VASwidth/2,center(2)+VASoffset,center(1)+VASwidth/2,center(2)+VASoffset);
    % Draw left major tick
    Screen('DrawLine',curWindow,white,center(1)-VASwidth/2,center(2)+VASoffset+20,center(1)-VASwidth/2,center(2)+VASoffset);
    % Draw right major tick
    Screen('DrawLine',curWindow,white,center(1)+VASwidth/2,center(2)+VASoffset+20,center(1)+VASwidth/2,center(2)+VASoffset);
    
    % % Draw minor ticks
    tickMark = center(1) + linspace(-VASwidth/2,VASwidth/2,n_steps);
    Screen('TextSize', curWindow, 24);
    tickLabels = {'1','2','3','4'};
    
    for tick = 1:length(tickLabels)
        Screen('DrawLine',curWindow,white,tickMark(tick),center(2)+VASoffset+10,tickMark(tick),center(2)+VASoffset);
        DrawFormattedText(curWindow,tickLabels{tick},tickMark(tick)-10,center(2)+VASoffset-30,white);
        DrawFormattedText(curWindow,tickLabelsWords{tick},tickMark(tick)-30,center(2)+VASoffset-60,white);
    end
    DrawFormattedText(curWindow,'Wie sicher?','center',center(2)+VASoffset+75,white);
    %    Screen('FillPoly',curWindow,white,arrowPoints);
    Screen('Flip', curWindow);
    
    
    [RT, keyCode]                   = KbPressWait(kbNum);
    deadline = 1;
    
    
end

if deadline == 0
    conf = NaN;
    RT_conf = NaN;
    % Draw confidence text
    DrawFormattedText(curWindow,'Too late!','center',center(2)+VASoffset+75,white);
    Screen('Flip', curWindow);
    pause(p.times.confFBDuration_inSecs);
    
elseif deadline == 1
    
    % if space is pressed-> error trial, abort
    if (find(keyCode) == responseKeys(1))
        
        conf = NaN;
        RT_conf = NaN;
        % Draw confidence text
        DrawFormattedText(curWindow,'Error trial / Falsche Eingabe','center',center(2)+VASoffset+75,white);
        Screen('Flip', curWindow);
        pause(p.times.confFBDuration_inSecs);
        
        
        
    else
        
        % keyCode - returns an array with 256 columns with 1 in the respective code column
        %conf = find(keyCode)-29; % "find(keyCode)-29;" for the regular keyboard
        if find(keyCode) == 14 % leftmost button in the top raw
            conf = 1;
        elseif find(keyCode) == 13
            conf = 2;
        elseif find(keyCode) == 11
            conf = 3;
        elseif find(keyCode) == 10
            conf = 4;
        end
            
        RT_conf = RT - start_time;
        
        % Change the color of the arrow depending on what mode it is
        if mode == 'm'
            arrowColor = white;
        elseif mode == 'c'
            if respCorr
                arrowColor = green;
            else
                arrowColor = red;
            end
        end
        
        %% Show confirmation arrow
        
        % Draw line
        Screen('DrawLine',curWindow,white,center(1)-VASwidth/2,center(2)+VASoffset,center(1)+VASwidth/2,center(2)+VASoffset);
        % Draw left major tick
        Screen('DrawLine',curWindow,white,center(1)-VASwidth/2,center(2)+VASoffset+20,center(1)-VASwidth/2,center(2)+VASoffset);
        % Draw right major tick
        Screen('DrawLine',curWindow,white,center(1)+VASwidth/2,center(2)+VASoffset+20,center(1)+VASwidth/2,center(2)+VASoffset);
        
        % % Draw minor ticks
        tickMark = center(1) + linspace(-VASwidth/2,VASwidth/2,n_steps);
        Screen('TextSize', curWindow, 24);
        tickLabels = {'1','2','3','4'};
        for tick = 1:length(tickLabels)
            Screen('DrawLine',curWindow,white,tickMark(tick),center(2)+VASoffset+10,tickMark(tick),center(2)+VASoffset);
            DrawFormattedText(curWindow,tickLabels{tick},tickMark(tick)-10,center(2)+VASoffset-30,white);
            DrawFormattedText(curWindow,tickLabelsWords{tick},tickMark(tick)-30,center(2)+VASoffset-60,white);
        end
        DrawFormattedText(curWindow,'Wie sicher?','center',center(2)+VASoffset+75,white);
        
        % Show arrow
        xpos = center(1) + steps_x(conf);
        arrowPoints = [([-0.5 0 0.5]'.*arrowwidth)+xpos ([1 0 1]'.*arrowheight)+center(2)+VASoffset];
        Screen('FillPoly',curWindow,arrowColor,arrowPoints);
        Screen('Flip', curWindow);
        pause(p.times.confFBDuration_inSecs);
    end
    
end
end