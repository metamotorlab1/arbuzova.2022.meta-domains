# Polina Arbuzova, and Pedro Espinosa.
# Preliminary data check for Internal-External Metacognitive Domains study (aka "Young Adults" - for historical reasons).

# READ ME
# This script analyses data from the memory and visual tasks in the 'young adults' experiment.
# It obtains the following measures for each subject from their trials data: accuracy, d', meta-d', and m-ratio
# It also filters out trials where the type I RT is < 200ms or > 8s
#
# This script uses data that has been extracted from matlab in csv files. (see: young_adults_csv_export.m)
# Go to section "Extract data" to change the working directory to your 'data_for_R' folder. This folder will have subfolders for each subject and 2
# csv files per subject (as was created by the matlab script).
#
# OUTPUT 
# 3 RDS files, each containing one data frame:
#   - criteria_data   ------- (accuracy, d', meta-d', m-ratio, response perc., filters for extreme d values, accuracy filter, final filter)
#   - meta_data_full  ------- (contains additional vars from the MLE model eg. HR and FA)
#   - nr_df           ------- (nR values)


library(tidyverse)
library(dplyr)
library(psych)
library(extraoperators)
#library(metaSDT)
source("/Users/polinaarbuzova/Dropbox/PhDs/Polina/YoungAdults/Analysis/Pedro Scripts/Young Adults Prelim Analysis/sdt_functions.R")


# Functions =====================================================



all_levels_in_NR <- function(nr) {
  #' Add count 0 to unused confidence levels
  #'
  #' This function is nested in NR_and_fit_model to make sure all confidence levels are counted.
  #' When a participant doesn't use an interval it is absent. The function adds the missing interval
  #' with a count = 0 so that the MLE model can be used.
  
  levels <- c(1, 2, 3, 4)
  index <- levels %in% nr$confidence
  index <- index == FALSE #Can you belive there isn't a %notin% function???
  
  if (TRUE %in% index) {
    c1 = levels[index]
    c2 = rep(0, length(c1))
    missing_levels <- tibble(confidence = c1, n = c2)
    
    nr <- bind_rows(nr, missing_levels)
  }
  return(nr)
}

NR_and_fit_model <- function(modality_type, model_method = "BFGS") {

  
  # S1
  NR_S1_1 <- filter(modality_type, stim_side == 1, response == 1)
  
  NR_S1_1 <- NR_S1_1 %>%
    group_by(confidence) %>%
    tally() %>%
    all_levels_in_NR() %>%
    arrange(desc(confidence))
  
  if (length(NR_S1_1$confidence) != 4) stop(sprintf("NR_S1_1 does not have exactly 4 values, subject = %s", i))
  
  NR_S1_2 <- filter(modality_type, stim_side == 1, response == 2)
  
  NR_S1_2 <- NR_S1_2 %>%
    group_by(confidence) %>%
    tally() %>%
    all_levels_in_NR() %>%
    arrange(confidence)
  # make sure order is ascending
  
  NR_S1 <- c(NR_S1_1$n, NR_S1_2$n)
  
  #Create collapsed version (into 2 confidence bins (high/low))
  correct_high <- NR_S1[1] + NR_S1[2]
  correct_low <- NR_S1[3] + NR_S1[4]
  incorrect_low <- NR_S1[5] + NR_S1[6]
  incorrect_high <- NR_S1[7] + NR_S1[8]
  NR_S1_collapsed <- c(correct_high, correct_low, incorrect_low, incorrect_high)
  
  # Make sure NR_S1 has 8 values
  if (length(NR_S1) != 8) stop(sprintf("NR_S1 does not have exactly 8 values, subject = %s", i))
  
  # S2
  NR_S2_1 <- filter(modality_type, stim_side == 2, response == 1)
  
  NR_S2_1 <- NR_S2_1 %>%
    group_by(confidence) %>%
    tally() %>%
    all_levels_in_NR() %>%
    arrange(desc(confidence))
  
  
  NR_S2_2 <- filter(modality_type, stim_side == 2, response == 2)
  
  
  NR_S2_2 <- NR_S2_2 %>%
    group_by(confidence) %>%
    tally() %>%
    all_levels_in_NR() %>%
    arrange(confidence)
  # make sure order is ascending
  
  NR_S2 <- c(NR_S2_1$n, NR_S2_2$n)
  
  #Create collapsed version (into 2 confidence bins)
  incorrect_high <- NR_S2[1] + NR_S2[2]
  incorrect_low <- NR_S2[3] + NR_S2[4]
  correct_low <- NR_S2[5] + NR_S2[6]
  correct_high <- NR_S2[7] + NR_S2[8]
  NR_S2_collapsed <- c(incorrect_high, incorrect_low, correct_low, correct_high)
  
  # Make sure NR_S2 has 8 values
  if (length(NR_S2) != 8) stop(sprintf("NR_S2 does not have exactly 8 values, subject = %s", i))
  
  ### Fit the model =====================================================================================
  if (model_method == "BFGS" | model_method == "Nelder-Mead"){
    
    # Try running model with all bin counts
    
    fit_MLE <- tryCatch({
      fit_meta_d_MLE(NR_S1, NR_S2, method_input = model_method)
    }, error = function(e){
      message("Model could not be fit. Error message:")
      message(e)
      message("Replaced values with 'FAILED'")
      error_msg = e
      
      meta_labels = c("da", "s", "meta_da", "M_diff", "M_ratio", "meta_ca", "t2ca_rS1", "t2ca_rS2", "logL", "est_HR2_rS1", "est_HR2_rS2", "est_FAR2_rS1", "est_FAR2_rS2", "obs_HR2_rS1", "obs_HR2_rS2", "obs_FAR2_rS1", "obs_FAR2_rS2")
      empty_df = data.frame(matrix(ncol = length(meta_labels), nrow = 0))
      filler_row = rep("FAILED", length(meta_labels))
      for (i in 1:3) {
        empty_df = rbind(empty_df, filler_row)
      }
      colnames(empty_df) = meta_labels
      return(empty_df)
    }
    )
    
    fit_MLE_collapsed <- fit_meta_d_MLE(NR_S1_collapsed, NR_S2_collapsed, method_input = model_method)
    
    # Values for collapsed bins are added to the same dataframe as the 4th row
    fit_MLE <- rbind(fit_MLE, fit_MLE_collapsed)
    # Add column to label if row is from collapsed bins to 2 levels
    fit_MLE$collapsed_bins_2_levels <- c(FALSE, FALSE, FALSE, TRUE)
    
    # Create object that contains the fit_MLE df and the nR values
    output = list(df = fit_MLE, nr1 = NR_S1, nr2 = NR_S2)
    
  } else if (model_method == "SSE"){
    fit_SSE = fit_meta_d_SSE(NR_S1, NR_S2)
    fit_SSE_collapsed = fit_meta_d_SSE(NR_S1_collapsed, NR_S2_collapsed)
    
    # Values for collapsed bins are added to the same dataframe as the 4th row
    fit_SSE <- rbind(fit_SSE, fit_SSE_collapsed)
    # Add column to label if row is from collapsed bins to 2 levels
    fit_SSE$collapsed_bins_2_levels <- c(FALSE, FALSE, FALSE, TRUE)
    
    # Create object that contains the fit_SSE df and the nR values
    output = list(df = fit_SSE, nr1 = NR_S1, nr2 = NR_S2)
  }
  
  return(output)
}

# Extract data ===========================================================

#setwd("/Users/polinaarbuzova/Dropbox/PhDs/Polina/YoungAdults/Analysis")
#D <- "/home/pedro/data/young_adults/data_for_R"
#D <- "/Users/pedro/code/data/metamotorlab/young_adults/data_for_R"
#D <- "/Volumes/Seagate Backup Plus Drive/Young Adults/data_for_R"
#D = "/Users/pedro/Documents/Humboldt Universität/Metamotor/young_adults/data_for_R/csv_files"
D = "/Users/polinaarbuzova/Dropbox/PhDs/Polina/YoungAdults/Analysis/Pedro Scripts/Young Adults Prelim Analysis/data_for_R/csv_files/"
#saving_dir <- "/home/pedro/data/young_adults/data_for_R"
saving_dir = "/Users/polinaarbuzova/Dropbox/PhDs/Polina/YoungAdults/Analysis/Pedro Scripts/Young Adults Prelim Analysis/data_for_R"
setwd(D)

#Get list of paths to files
files = list.files(recursive = T)

#Create empty df
main_data = data.frame()

#Create for loop to extract csv files and bind them to main_data
for (target_file in files) {
  target_path = file.path(D, target_file)
  temp_data = read.csv(target_path, header = TRUE)
  
  #Determine if Visual or Memory and add label
  experiment <- strsplit(target_file, "_") %>% unlist()
  if (experiment[3] == "memory.csv") {
    modality_label = "memory"
  } else if (experiment[3] == "visual.csv") {
    modality_label = "visual"
  }
  
  #Add modality label
  temp_data[, "modality"] <- modality_label
  
  #Bind data to main data frame
  main_data = rbind(main_data, temp_data)
}

# Read motor task csv
setwd("..")
motor_data = read.csv(file = "young_adults_motor_task_fixed_conf.csv")
motor_data =  motor_data[ ,c(1:18, 1020:ncol(motor_data))] #Cleans up the 1002 columns called duration
motor_data = rename(motor_data, run = block, modality = condition, RT_conf = confidenceRT, accuracy = correct, response = chosenLever)
#
motor_data_full = motor_data
motor_data = motor_data[, c(1, 3, 20, 19, 7, 6, 8, 9, 4)]

# Criteria and measures ===============================================

### Filter out trials that are <200ms or >8s for type I RTs
main_data_unfiltered <- main_data
main_data <- main_data[main_data$RT > 0.200 & main_data$RT < 8, ]

motor_data_unfiltered = motor_data
motor_data = motor_data[motor_data$RT > 0.200 & motor_data$RT < 8, ]

# Record which subjects had trials excluded and how many for each task
excluded_trials = main_data_unfiltered[main_data_unfiltered$RT <= 0.200 | main_data_unfiltered$RT >= 8, ]
excluded_trials_motor = motor_data_unfiltered[motor_data_unfiltered$RT <= 0.200 | motor_data_unfiltered$RT >=8,]

subjects_excluded_trials = unique(excluded_trials$subject)
subjects_excluded_trials_motor = unique(excluded_trials_motor$subject)
subjects_excluded_trials = sort(unique(c(subjects_excluded_trials, subjects_excluded_trials_motor)))

excluded_trials_memory = c()
excluded_trials_visual = c()
excluded_trials_motor_task = c()
excluded_trials_visuomotor = c()
excluded_trials_overall = c()
total_trials_memory = c()
total_trials_visual = c()
total_trials_motor = c()
total_trials_visuomotor = c()
total_trials_overall = c()


# Create the vectors
for (i in subjects_excluded_trials) {
  excluded_trials_memory = c(excluded_trials_memory,
                             nrow(excluded_trials[excluded_trials$subject == i & excluded_trials$modality == "memory",]))
  excluded_trials_visual = c(excluded_trials_visual,
                             nrow(excluded_trials[excluded_trials$subject == i & excluded_trials$modality == "visual",]))
  excluded_trials_motor_task = c(excluded_trials_motor_task,
                                 nrow(excluded_trials_motor[excluded_trials_motor$subject == i & excluded_trials_motor$modality == "motor",]))
  excluded_trials_visuomotor = c(excluded_trials_visuomotor,
                                 nrow(excluded_trials_motor[excluded_trials_motor$subject == i & excluded_trials_motor$modality == "visuomotor",]))
  total_trials_memory = c(total_trials_memory,
                          nrow(main_data_unfiltered[main_data_unfiltered$subject == i & main_data_unfiltered$modality == "memory",]))
  total_trials_visual = c(total_trials_visual,
                          nrow(main_data_unfiltered[main_data_unfiltered$subject == i & main_data_unfiltered$modality == "visual",]))
  total_trials_motor =  c(total_trials_motor,
                          nrow(motor_data_unfiltered[motor_data_unfiltered$subject == i & motor_data_unfiltered$modality == "motor",]))
  total_trials_visuomotor = c(total_trials_visuomotor,
                              nrow(motor_data_unfiltered[motor_data_unfiltered$subject == i & motor_data_unfiltered$modality == "visuomotor",]))
}

# Add totals
excluded_trials_overall = excluded_trials_memory + excluded_trials_visual + excluded_trials_motor_task + excluded_trials_visuomotor
total_trials_overall = total_trials_memory + total_trials_visual + total_trials_motor + total_trials_visuomotor

excluded_trials_data = data.frame("subject" = subjects_excluded_trials,
                                  "n_trials_memory" = total_trials_memory,
                                  "n_excluded_memory" = excluded_trials_memory,
                                  "n_trials_visual" = total_trials_visual,
                                  "n_excluded_visual" = excluded_trials_visual,
                                  "n_trials_motor" = total_trials_motor,
                                  "n_excluded_motor" = excluded_trials_motor_task,
                                  "n_trials_visuomotor" = total_trials_visuomotor,
                                  "n_excluded_visuomotor" = excluded_trials_visuomotor,
                                  "n_trials_overall" = total_trials_overall,
                                  "n_excluded_overall" = excluded_trials_overall
)


perc_excluded_overall = (excluded_trials_data$n_excluded_overall / excluded_trials_data$n_trials_overall)*100
perc_excluded_memory = (excluded_trials_data$n_excluded_memory / excluded_trials_data$n_trials_memory)*100
perc_excluded_visual = (excluded_trials_data$n_excluded_visual / excluded_trials_data$n_trials_visual)*100
perc_excluded_motor = (excluded_trials_data$n_excluded_motor / excluded_trials_data$n_trials_motor)*100
perc_excluded_visuomotor = (excluded_trials_data$n_excluded_visuomotor / excluded_trials_data$n_trials_visuomotor)*100

excluded_trials_data = cbind(excluded_trials_data, perc_excluded_overall, perc_excluded_memory, perc_excluded_visual, perc_excluded_motor, perc_excluded_visuomotor)

### For every subject and experiment, obtain measures (percentage correct (accuracy), d’, meta-d’ and m-ratio, and nRs)

## Create data frame to store measures
n_subjects = unique(main_data$subject)
modality = c("memory", "visual", "motor", "visuomotor")

#Creates a df with as many rows as there are combinations of subject and modalities
criteria_data <- expand.grid(modality, n_subjects)[, c(2,1)] #last bit reverses order
names(criteria_data)[1] <- "subject"
names(criteria_data)[2] <- "modality"

#Adds empty columns to the df so the other measures can be added
nans <- rep(NaN, nrow(criteria_data))
criteria_data$accuracy = nans
criteria_data$d = nans
criteria_data$meta_d = nans
criteria_data$m_ratio = nans
criteria_data$response1_perc = nans
criteria_data$response2_perc = nans

# Create separate data frame to store all meta measures
meta_data_full <- data.frame(subject = rep(n_subjects, each = 4*length(modality)))
meta_data_full$modality <- rep(rep(modality, each = 4), times = length(n_subjects))
name_vector <- c("da", "s", "meta_da", "M_diff", "M_ratio", "meta_ca", "t2ca_rS1", "t2ca_rS2", "logL", "est_HR2_rS1", "est_HR2_rS2", "est_FAR2_rS1", "est_FAR2_rS2", "obs_HR2_rS1", "obs_HR2_rS2", "obs_FAR2_rS1", "obs_FAR2_rS2", "collapsed_bins_2_levels", "d_prime_t1", "c_raw_t1", "c_prime_t1", "s1_HR", "s1_FA", "filter_type2_extremes", "filter_type1_extremes")
for (i in name_vector){
  meta_data_full[, i] <- NA
}

#Create separate data frame for nr measures
nr_df = data.frame(subject = criteria_data$subject, modality = criteria_data$modality, NR_S1_1 = NA, NR_S1_2 = NA, NR_S1_3 = NA, NR_S1_4 = NA, NR_S1_5 = NA, NR_S1_6 = NA, NR_S1_7 = NA, NR_S1_8 = NA,
                   NR_S2_1 = NA, NR_S2_2 = NA, NR_S2_3 = NA, NR_S2_4 = NA, NR_S2_5 = NA, NR_S2_6 = NA, NR_S2_7 = NA, NR_S2_8 = NA)



## Main For loop ========================================================================================================================================================================
#Create for loop to calculate measures by subject and modality

# But first incorporate motor_data_selection to main_data
main_data = rbind(main_data, motor_data)
main_data = main_data[order(main_data$subject),]

# Trying out without the subjects that have bugs with the MLE model
# main_data_og = main_data
# main_data = main_data[main_data$subject %!in% c(10, 27, 34, 39, 6, 9),]
#main_data = main_data[main_data$subject != 2 & main_data$subject != 10 & main_data$subject != 27 & main_data$subject != 34 & main_data$subject != 39 & main_data$subject != 6 & main_data$subject != 9, ]
#revert
main_data = main_data_og

#Specify the models
#model = c("BFGS", "Nelder-Mead", "SSE")
model = c("BFGS")

for (m in model) {
  for (i in n_subjects) {
    print(i)
    
    # Separate data by modality (task)
    subject_indexer = main_data$subject == i
    subject_data = main_data[subject_indexer,]
    subject_data_memory = subject_data[subject_data$modality == "memory", ]
    subject_data_visual = subject_data[subject_data$modality == "visual", ]
    subject_data_motor = subject_data[subject_data$modality == "motor", ]
    subject_data_visuomotor = subject_data[subject_data$modality == "visuomotor", ]
    
    # Abstract version for each experiment
    for (task in modality){
      
      # Select the data of the current task
      if (task == "memory"){
        task_data = subject_data_memory
      } else if (task == "visual"){
        task_data = subject_data_visual
      } else if (task == "motor"){
        task_data = subject_data_motor
      } else if (task == "visuomotor"){
        task_data = subject_data_visuomotor
      }
      
      
      #Check if data is empty to skip or analyse
      if (nrow(task_data) > 0) {
        
        #storage indeces
        cd_task_id <- criteria_data$subject == i & criteria_data$modality == task
        
        
        ## Check the accuracy
        perc_corr <- task_data %>%
          summarize(percent = length(accuracy[accuracy == "1"])/n())
        
        # Store accuracy
        criteria_data$accuracy[cd_task_id] <- perc_corr
        
        ## Count responses (left/right)
        perc_responses <- task_data %>%
          summarize(response1_perc = length(response[response == "1"])/n())
        perc_responses <- cbind(perc_responses, response2_perc = 1 - perc_responses[1, 1])
        
        # Store response percentages
        criteria_data$response1_perc[cd_task_id] = perc_responses[1, 1]
        criteria_data$response2_perc[cd_task_id] = perc_responses[1, 2]
        
        ## fit MLE model to get metacognition measures
        meta_nr_data = NR_and_fit_model(task_data, model_method = m)
        meta_data_task <- meta_nr_data$df                 #selects only the df from the object
        print(paste(task, "was fit"))
        
        # Store measures in criteria_data data.frame
        criteria_data$d[cd_task_id] <- meta_data_task$da[1]
        criteria_data$meta_d[cd_task_id] <- meta_data_task$meta_da[1]
        criteria_data$m_ratio[cd_task_id] <- meta_data_task$M_ratio[1]
        
        #Store nr measures in nr_df
        nr_task_id <- nr_df$subject == i & nr_df$modality == task
        nr1_as_df = as.data.frame(t(meta_nr_data$nr1))
        nr2_as_df = as.data.frame(t(meta_nr_data$nr2))
        nr_df[nr_task_id, 3:10] = nr1_as_df
        nr_df[nr_task_id, 11:18] = nr2_as_df
        
        # Obtain type I measures
        task_data_formatted <- sdt_counts(task_data, stimulus = stim_side, response = response, split_resp = FALSE)
        type_1_measures_task <- type_1_sdt(task_data_formatted, stimulus = stim_side, response = response, counts = total)
        
        #bind them to meta_data_task
        meta_data_task <- cbind(meta_data_task, type_1_measures_task)
        
        # Create empty columns of filters
        meta_data_task$filter_type2_extremes <- NA
        meta_data_task$filter_type1_extremes <- NA
        
        # Filter of type I and II extreme values of collapsed bins data. Exclude if  0.05 > value > 0.95
        # TRUE is for valid data, FALSE for excluded data
        # Type II collapsed bins
        type2_columns <- c("obs_HR2_rS1", "obs_HR2_rS2", "obs_FAR2_rS1", "obs_FAR2_rS2")
        sub_meta_data_task <- meta_data_task %>% select(all_of(type2_columns))
        
        if (any(sub_meta_data_task[4,] <= 0.95) | any(sub_meta_data_task[4,] >= 0.05)) {
          meta_data_task$filter_type2_extremes[4] <- TRUE
        } else if (any(sub_meta_data_task[4] > 0.95 | any(sub_meta_data_task[4] < 0.05))) {
          meta_data_task$filter_type2_extremes[4] <- FALSE
        }         #Did not use plain else in case of NA
        
        # Type I
        type1_columns <- c("d_prime", "c_raw", "c_prime", "s1_HR", "s1_FA")
        sub_type1_data <- meta_data_task %>% select(all_of(type1_columns))
        if (any(sub_type1_data[1, 4:5] <= 0.95) | any(sub_type1_data[1, 4:5] >= 0.05)) {
          meta_data_task$filter_type1_extremes <- TRUE
        } else if (any(sub_type1_data[1] > 0.95) | any(sub_type1_data[1] < 0.05)) {
          meta_data_task$filter_type1_extremes <- FALSE
        } 
        
        # Store all meta measures in meta_data_full
        mdf_task_id <- meta_data_full$subject == i & meta_data_full$modality == task
        meta_data_full[mdf_task_id, 3:ncol(meta_data_full)] <- meta_data_task
        
        # Store meta filters in criteria_data
        criteria_data$filter_type2_extremes[cd_task_id] <- meta_data_full$filter_type2_extremes[mdf_task_id][4]
        criteria_data$filter_type1_extremes[cd_task_id] <- meta_data_full$filter_type1_extremes[mdf_task_id][4]
      }
      
    }
    
    
    
    # Check d' and meta-d' values. (To corroborate methods)
    data_check = meta_data_full[0, c(1, 2, 3, 5, 7)]
    for (i in n_subjects) {
      for (mdlty in modality) {
        values = meta_data_full[meta_data_full$subject == i & meta_data_full$modality == mdlty, c(1, 2, 3, 5, 7)][1,]
        data_check = rbind(data_check, values)
      }
    }
    
    if(m == "Nelder-Mead"){
      summary_NM = describeBy(data_check, data_check$modality)
      data_check_NM = data_check
      save(data_check_NM, summary_NM, file = 'data_check_NM')
    } else if (m == "SSE"){
      summary_SSE = describeBy(data_check, data_check$modality)
      data_check_SSE = data_check
      save(data_check_SSE, summary_SSE, file = 'data_check_SSE')
    } else if (m == "BFGS"){
      summary_BFGS = describeBy(data_check, data_check$modality)
      data_check_BFGS = data_check
      save(data_check_BFGS, summary_BFGS, file = 'data_check_BFGS')
    }
  }
}   

# Filtering =================================================================================

# Clean out NaNs, namely subject 33 memory (and some subjects without skittles)
 criteria_data <- na.omit(criteria_data)
 nr_df = na.omit(nr_df)
 
# Bind nr_df to criteria_data to have a complete df
 if (all(nr_df$subject == criteria_data$subject, nr_df$modality == criteria_data$modality)) {
   criteria_data = cbind(criteria_data, nr_df[, 3:ncol(nr_df)])
 }

## Accuracy Filter
# "For each participant, any individual conditions will be excluded if Type I accuracy in these tasks is under 60% or above 80%."

  # Add column called filter_accuracy.
  criteria_data['filter_accuracy'] <- rep(TRUE, times = length(criteria_data[,1]))

  # Label violations as false
  # TRUE signifies that the data satisfies inclusion criteria (60 =< acc <= 80) FALSE means it does not.
  for (i in 1:length(criteria_data[,1])) {
    if (criteria_data$accuracy[i] < 0.60 | criteria_data$accuracy[i] > 0.80){
      criteria_data$filter_accuracy[i] <- FALSE
    }
    }

## Type II extreme values filter (TRUE signifies included, FALSE that it violates the criterion)
    #  For each participant, any individual conditions will be excluded if, for the Type II task, when
    #  confidence level is divided into two bins, any extreme values for Type I or II hit rates (HR)
    #  and  false  alarm  rates  (FAR),  (<0.05 or  >0.95)

        # This column was actually created during the loop, it appears under each modality calculations under: "Filter of type II extreme values of collapsed bins data."
  

## Final judgement filter (muahahaaa!)
  # create column "filter_all_criteria" that is TRUE only for those who satisfy all conditions
  for (i in 1:nrow(criteria_data)){
    if (all(criteria_data$filter_type2_extremes[i] == TRUE, criteria_data$filter_type1_extremes[i] == TRUE, criteria_data$filter_accuracy[i] == TRUE)){
      criteria_data$filter_all_criteria[i] <- TRUE
    } else {
      criteria_data$filter_all_criteria[i] <- FALSE
    }
  }

  
#Troubleshooting
  #troublemakers = nr_df[nr_df$subject %in% c(10, 27, 34, 39, 6, 9) & nr_df$modality %in% c("motor", "visuomotor"),]
  #troublemakers = troublemakers[c(1, 4, 5, 8, 10, 12),]
  
  #copy meta_data_full, then clean it
  meta_data_full_original = meta_data_full
  meta_data_full[c("filter_type2_extremes", "filter_type1_extremes")][is.na(meta_data_full[c("filter_type2_extremes", "filter_type1_extremes")])] <- FALSE
  meta_data_full = na.omit(meta_data_full)
  #revert
  meta_data_full = meta_data_full_original
  
  interest_col = c(1:3, 5:7, 20, 21)
  
  troublemakers = unique(meta_data_full$subject[meta_data_full$da == "FAILED"])
  
  meta_data_trouble = meta_data_full[meta_data_full$subject == 10 | meta_data_full$subject == 27 |meta_data_full$subject == 34 |meta_data_full$subject ==39 |meta_data_full$subject ==6 |meta_data_full$subject ==9,]
  meta_data_trouble_collapsed = meta_data_trouble[meta_data_trouble$collapsed_bins_2_levels == TRUE, interest_col]
  #meta_data_full[meta_data_full$subject == troublemakers,]
  
  #variable with troublemaker number and failed task
  troublemaker_and_task = c(meta_data_full$subject[meta_data_full$da == "FAILED"], meta_data_full$modality[meta_data_full$da == "FAILED"][1])
  troublemaker_and_task = data.frame("subject" = NA, "modality" = NA)
  troublemaker_meta_data = colnames(meta_data_full)
  
  for (t in troublemakers) {
    failed_task = meta_data_full$modality[meta_data_full$subject == t & meta_data_full$da == "FAILED"][1]
    troublemaker_and_task = rbind(troublemaker_and_task, c(t, failed_task))
    
    troublemaker_meta_data = rbind(troublemaker_meta_data, meta_data_full[meta_data_full$subject == t & meta_data_full$modality == failed_task,])
  }

  troublemaker_and_task = na.omit(troublemaker_and_task)
  troublemaker_meta_data = troublemaker_meta_data[-1,]
  
  troublemaker_meta_collapsed = troublemaker_meta_data[troublemaker_meta_data$collapsed_bins_2_levels == TRUE,]
  
  for (t in vector) {
    
  }
  meta_data_full[meta_data_full$subject ]
  

  
# Save the data frames =======
# saving dir must be specified at the beginning of section "extract data"
setwd(saving_dir)
saveRDS(criteria_data, file = 'young_adults_criteria_data')
saveRDS(meta_data_full, file = 'young_adults_meta_data_full')
saveRDS(nr_df, file = 'young_adults_nr_data_frame')
saveRDS(excluded_trials_data, file = 'young_adults_excluded_trials_data')
saveRDS(main_data, file = 'young_adults_main_data')
saveRDS(main_data_unfiltered, file = 'young_adults_main_data_unfiltered')



